{
  description = "My nixos config (based on misterio77#standard template)";

  inputs = {
    # Nixpkgs
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # You can access packages and modules from different nixpkgs revs
    # at the same time. Here's an working example:
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.11";
    # Also see the 'stable-packages' overlay at 'overlays/default.nix'.

    # Home manager
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:nixos/nixos-hardware";
    impermanence.url = "github:nix-community/impermanence";
    nixvim.url = "github:nix-community/nixvim";
    nixvim.inputs.nixpkgs.follows = "nixpkgs";
    agenix.url = "github:ryantm/agenix";
    agenix-rekey.url = "github:oddlama/agenix-rekey";
    agenix-rekey.inputs.nixpkgs.follows = "nixpkgs";
    # xob-tools.url = "gitlab:joWeiss/xob-tools";
    # xob-tools.inputs.nixpkgs.follows = "nixpkgs";

    srvos.url = "github:nix-community/srvos";
    nixpkgs-srv.follows = "srvos/nixpkgs";
    deploy-rs.url = "github:serokell/deploy-rs";

    # Shameless plug: looking for a way to nixify your themes and make
    # everything match nicely? Try nix-colors!
    # nix-colors.url = "github:misterio77/nix-colors";
  };

  outputs =
    {
      self,
      nixpkgs,
      home-manager,
      ...
    }@inputs:
    let
      inherit (self) outputs;
      # Supported systems for your flake packages, shell, etc.
      systems = [
        "aarch64-linux"
        "x86_64-linux"
      ];
      specialArgs = {
        # put inputs here to access in my modules
        inherit self inputs outputs;
      };
      # This is a function that generates an attribute by calling a function you
      # pass to it, with each system as an argument
      forAllSystems = nixpkgs.lib.genAttrs systems;
    in
    {
      agenix-rekey = inputs.agenix-rekey.configure {
        userFlake = self;
        nodes = self.nixosConfigurations;
      };
      devShells = forAllSystems (
        system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
        in
        {
          default = pkgs.mkShell {
            packages = [
              pkgs.lazygit
              pkgs.helix
              pkgs.nil
              pkgs.nixfmt-rfc-style
              pkgs.deploy-rs
              pkgs.nixd
              pkgs.just
            ];
          };
        }
      );
      # Your custom packages
      # Accessible through 'nix build', 'nix shell', etc
      packages = forAllSystems (system: import ./pkgs nixpkgs.legacyPackages.${system});

      # Formatter for your nix files, available through 'nix fmt'
      # Other options beside 'alejandra' include 'nixpkgs-fmt'
      formatter = forAllSystems (system: nixpkgs.legacyPackages.${system}.nixfmt-rfc-style);

      # Your custom packages and modifications, exported as overlays
      overlays = import ./overlays { inherit inputs; };

      # Reusable nixos modules you might want to export
      # These are usually stuff you would upstream into nixpkgs
      nixosModules = import ./modules/nixos;
      # Reusable home-manager modules you might want to export
      # These are usually stuff you would upstream into home-manager
      homeManagerModules = import ./modules/home-manager;

      # NixOS configuration entrypoint
      # Available through 'nixos-rebuild --flake .#your-hostname'
      nixosConfigurations = {
        nixosDesktop = nixpkgs.lib.nixosSystem {
          inherit specialArgs;
          modules = [
            ./nixos/nixosDesktop/configuration.nix
            inputs.disko.nixosModules.disko
            inputs.nixos-hardware.nixosModules.common-cpu-amd-pstate
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.jonas = import ./home-manager/desktop/home.nix;
              home-manager.extraSpecialArgs = specialArgs // {
                myStateVersion = "22.05";
              };
              home-manager.sharedModules = [ ];
            }
          ];
        };
        nix280 = nixpkgs.lib.nixosSystem {
          inherit specialArgs;
          modules = [
            inputs.impermanence.nixosModules.impermanence
            inputs.nixos-hardware.nixosModules.lenovo-thinkpad-x280
            # > Our main nixos configuration file <
            ./nixos/nix280/configuration.nix
            # > Home manager as nixos module <
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.jonas = import ./home-manager/laptop/home.nix;
              home-manager.extraSpecialArgs = specialArgs // {
                myStateVersion = "22.11";
              };
              home-manager.sharedModules = [ ];
            }
          ];
        };
        olymp = inputs.nixpkgs-srv.lib.nixosSystem {
          inherit specialArgs;
          system = "x86_64-linux";
          modules = [
            inputs.impermanence.nixosModules.impermanence
            inputs.disko.nixosModules.disko
            inputs.srvos.nixosModules.server
            inputs.srvos.nixosModules.mixins-nix-experimental
            inputs.srvos.nixosModules.mixins-systemd-boot
            inputs.srvos.nixosModules.mixins-terminfo
            inputs.srvos.nixosModules.mixins-trusted-nix-caches
            ./nixos/olymp/configuration.nix
          ];
        };
        coruscant = inputs.nixpkgs-srv.lib.nixosSystem {
          inherit specialArgs;
          system = "x86_64-linux";
          modules = [
            inputs.disko.nixosModules.disko
            inputs.srvos.nixosModules.server
            inputs.srvos.nixosModules.mixins-nix-experimental
            inputs.srvos.nixosModules.mixins-terminfo
            inputs.srvos.nixosModules.mixins-trusted-nix-caches
            inputs.srvos.nixosModules.mixins-nginx

            ./nixos/coruscant/configuration.nix
          ];
        };
      };

      homeConfigurations = {
        "jonas.weissensel@S0158394" = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs {
            system = "aarch64-darwin";
            overlays = [
              outputs.overlays.additions
              outputs.overlays.modifications
              outputs.overlays.stable-packages
            ];
          };
          extraSpecialArgs = {
            inherit inputs outputs;
            myStateVersion = "23.11";
          };
          modules = [
            ./home-manager/work/otto-macbook.nix
            (
              { pkgs, ... }@args:
              {
                nix.package = pkgs.nixVersions.latest;
                nix.registry.nixpkgs.flake = args.inputs.nixpkgs;
                nix.registry.nixos = {
                  from = {
                    type = "indirect";
                    id = "nixos";
                  };
                  flake = args.inputs.nixpkgs;
                };
                # nix.extraOptions = ''
                # include ${./home-manager/work/gh-token.txt}
                # '';
              }
            )
          ];
        };
        "joweisse@S0147099" = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs {
            system = "x86_64-linux";
            overlays = [
              outputs.overlays.additions
              outputs.overlays.modifications
              outputs.overlays.stable-packages
            ];
          };
          extraSpecialArgs = {
            inherit inputs outputs;
            myStateVersion = "23.11";
          };
          modules = [
            ./home-manager/work/otto.nix
            (
              { pkgs, ... }@args:
              {
                nix.package = pkgs.nixVersions.latest;
                nix.registry.nixpkgs.flake = args.inputs.nixpkgs;
                nix.registry.nixos = {
                  from = {
                    type = "indirect";
                    id = "nixos";
                  };
                  flake = args.inputs.nixpkgs;
                };
                nix.extraOptions = ''
                  include ${./home-manager/work/gh-token.txt}
                '';
              }
            )
          ];
        };
      };

      deploy.nodes = {
        olymp.hostname = "olymp";
        olymp.profiles.system = {
          user = "root";
          sshUser = "jonas";
          # sshOpts = [ "-i" "~/.ssh/id_ed25519_nixdesktop" ];
          path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos self.nixosConfigurations.olymp;
        };
        coruscant.hostname = "85.215.144.93";
        coruscant.profiles.system = {
          user = "root";
          sshUser = "jonas";
          sshOpts = [
            "-i"
            "~/.ssh/id_ed25519_nixdesktop"
          ];
          path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos self.nixosConfigurations.coruscant;
        };
      };

      checks = builtins.mapAttrs (
        system: deployLib: deployLib.deployChecks self.deploy
      ) inputs.deploy-rs.lib;
    };
}
