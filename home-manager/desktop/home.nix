{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  myStateVersion,
  ...
}:
{
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    outputs.homeManagerModules.nixvim
    outputs.homeManagerModules.mkHome
  ];
  myconfig = {
    mkHome = {
      enable = true;
      isNixOs = true;
      isPrivateDevice = true;
      useGnome = true;
      username = "jonas";
      sessionVariables = {
        EDITOR = "hx";
        SSH_AUTH_SOCK = "/run/user/1000/keyring/ssh";
      };
      stateVersion = myStateVersion;
      extraPackages = [
        # for LaTeX
        pkgs.tectonic
        pkgs.texlab
        # for Photography
        pkgs.shotwell
        # for Communication
        pkgs.teamspeak_client
        pkgs.discord
        pkgs.zoom-us
        pkgs.chromium
        # a windows gaming helper/manager
        pkgs.lutris

        # multimedia
        pkgs.supersonic

        # Utilities
        pkgs.audacious
        pkgs.ntfs3g
        pkgs.mosh
      ];

      programs = {
        kitty.enable = true;
        kitty.font = {
          name = "BlexMono Nerd Font";
          size = 12;
        };
        kitty.settings = {
          scrollback_lines = 50000;
          update_check_interval = 0;
        };
        kitty.theme = "Catppuccin-Mocha";
        git = {
          userName = "Jonas Weissensel";
          userEmail = "gitlab@jonasweissensel.de";
        };
        rofi.enable = true;
      };
    };
    programs.nixvim.enable = true;
  };
}
