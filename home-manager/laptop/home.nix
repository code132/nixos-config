{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  myStateVersion,
  ...
}:
{
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    outputs.homeManagerModules.nixvim
    outputs.homeManagerModules.mkHome
  ];
  myconfig = {
    mkHome = {
      enable = true;
      isNixOs = true;
      isPrivateDevice = true;
      useCustomWm = true;
      username = "jonas";
      sessionVariables = {
        EDITOR = "hx";
        MOZ_ENABLE_WAYLAND = "1";
      };
      stateVersion = myStateVersion;
      extraPackages = with pkgs; [
        # kde stuff
        aha
        # cli
        pfetch
        grim
        slurp
        swappy
        dmenu-rs
        maim
        (writeShellApplication {
          name = "river-autostart";
          runtimeInputs = [
            networkmanagerapplet
            busybox
            config.programs.waybar.package
          ];
          text = ''
            killall -q waybar
            waybar -c ~/.config/waybar/config -s ~/.config/waybar/style.css
          '';
        })
        (writeShellApplication {
          name = "wayland-lock";
          runtimeInputs = [swaylock];
          text = ''
            MUTEX="''${HOME}/.config/lock/river-lock"
            LOG="''${HOME}/.config/lock/lock-log"
            NOLOCK="''${HOME}/.config/lock/NOLOCK"

            img=$(find "''${HOME}"/.config/river/backgrounds -type f | shuf -n 1)

            VERSION="0.9"

            log () {
            	when=$(date "+%Y-%m-%d %H:%M:%S")
            	msg="[lock ''${VERSION}] ''${when} $1"
            	echo "''${msg}" >> "''${LOG}"
            }

            lock () {
            	if [ ! -f "''${NOLOCK}" ]; then
            		swaylock -F -l -i "$img"
            	else
            		log "''${NOLOCK} found, not locking"
            	fi
            }

            if [ "$1" = force ]; then
            	log "Forcing lock, removing ''${NOLOCK} and ''${MUTEX}"
            	rm -rf "''${NOLOCK}"
            	rm -rf "''${MUTEX}"
            fi

            if mkdir "$MUTEX"; then
            	log "Successfully acquired lock"

            	trap 'rm -rf "$MUTEX"' 0	# remove mutex when script finishes

            	lock
            else
            	log "cannot acquire lock, giving up on $MUTEX"
            	exit 0
            fi;
          '';
        })
        (writeShellScriptBin "wayland-leave" ''
          choice=$(printf "Lock\nLogout\nSuspend\nReboot\nShutdown" | rofi -dmenu)
          if [[ $choice == "Lock" ]];then
              wayland_session_lock
          elif [[ $choice == "Logout" ]];then
              # pkill -KILL -u "$USER"
              riverctl exit
          elif [[ $choice == "Suspend" ]];then
              systemctl suspend
          elif [[ $choice == "Reboot" ]];then
              systemctl reboot
          elif [[ $choice == "Shutdown" ]];then
              systemctl poweroff
          fi
        '')
        # gui
        nautilus-python
        discord
      ];

      programs = {
        feh.enable = true;
        foot = {
          enable = true;
          server.enable = true;
          settings = {
            main = {
              font = "BlexMono Nerd Font:size=8";
              dpi-aware = true;
              bold-text-in-bright = "palette-based";
            };
            bell = {
              urgent = true;
              visual = true;
              notify = true;
            };
            csd = {
              # client side decorations
              hide-when-maximized = true;
            };
          };
        };
        git = {
          userName = "Jonas Weissensel";
          userEmail = "gitlab@jonasweissensel.de";
        };
        qutebrowser = {
          enable = true;
          quickmarks = {
            github = "https://github.com";
            gitlab = "https://gitlab.com";
            codeberg = "https://codeberg.org";
            gh-nixpkgs = "https://github.com/NixOS/nixpkgs";
            hm-options = "https://nix-community.github.io/home-manager/options.html";
            home-manager = "https://github.com/nix-community/home-manager";
            nixpkgs = "https://nixos.org/manual/nixpkgs/unstable/";
            nix = "https://nixos.org/manual/nix/unstable/";
            "nix.dev" = "https://nix.dev/";
            search = "https://search.nixos.org/packages";
            youtube = "https://youtube.com";
          };
          searchEngines = {
            g = "https://www.google.com/search?q={}";
            ddg = "https://www.duckduckgo.com/?q={}";
          };
        };
        rofi.enable = true;
        waybar = {
          enable = true;
          systemd.enable = false;
          style = builtins.readFile ./river-style.css;
          settings = {
            mainBar = {
              mode = "dock";
              layer = "top";
              position = "top";
              margin = "6";
              spacing = "4";
              # height = 16;
              modules-left = ["network" "cpu" "memory" "tray"];
              modules-center = ["river/tags"];
              modules-right = ["backlight" "battery" "clock" "custom/power"];
              "river/tags" = {"num-tags" = 9;};
              backlight = {
                tooltip = false;
                interval = 1;
                format = " {}%";
                on-scroll-up = "${pkgs.brightnessctl}/bin/brightnessctl s 5%+";
                on-scroll-down = "${pkgs.brightnessctl}/bin/brightnessctl s 5%-";
              };
              battery = {
                states = {
                  good = 95;
                  warning = 30;
                  critical = 20;
                };
                "format" = "{icon}  {capacity}%";
                "format-charging" = "🗲 {capacity}%";
                "format-plugged" = " {capacity}%";
                "format-alt" = "{time} {icon}";
                "format-icons" = ["" "" "" "" ""];
              };
              tray = {
                icon-size = 18;
                spacing = 10;
              };
              clock = {format = "{: %I:%M %p | %d/%m/%Y}";};
              cpu = {
                interval = 15;
                format = " {}%";
                max-length = 10;
              };
              memory = {
                interval = 30;
                format = " {}%";
                max-length = 10;
              };
              network = {
                # interface = "wlp2*", // (Optional) To force the use of this interface
                format-wifi = "{essid} ({signalStrength}%)  ";
                format-ethernet = "{ipaddr}/{cidr} ";
                tooltip-format = "{ifname} via {gwaddr} ";
                format-linked = "{ifname} (No IP) ";
                format-disconnected = "Disconnected ⚠";
                format-alt = "{ifname}: {ipaddr}/{cidr}";
              };
              "custom/power" = {
                "format" = " ";
                "on-click" = "wayland-leave";
              };
            };
          };
        };
      };
      services = {dunst.enable = true;};
      systemd = {
        user.services = {
          betterlockscreen = {
            Unit = {
              Description = "Lock screen when going to sleep/suspend";
              Before = ["sleep.target" "suspend.target"];
            };
            Service = {
              # User = "%I";
              Type = "simple";
              Environment = "DISPLAY=:0";
              ExecStart = "${pkgs.betterlockscreen}/bin/betterlockscreen --lock";
              TimeoutSec = "infinity";
              ExecStartPost = "${pkgs.coreutils}/bin/sleep 1";
            };
            Install = {WantedBy = ["sleep.target" "suspend.target"];};
          };
          xob-read-brightness = {
            Unit = {Description = "Read brightness and show an overlay";};
            Install = {WantedBy = ["default.target"];};
            Service = {
              ExecStart = "${pkgs.writeShellScript "xob-read-brightness" ''
                /run/current-system/sw/bin/read-brightness | ${pkgs.xob}/bin/xob
              ''}";
            };
          };
        };
      };
    };
    programs.nixvim.enable = true;
  };
}
