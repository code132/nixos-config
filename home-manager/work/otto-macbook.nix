{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  myStateVersion,
  ...
}:
{
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    outputs.homeManagerModules.nixvim
    outputs.homeManagerModules.mkHome
  ];
  xdg.configFile = {
    "borders.bordersrc" = {
      source =
        let
          rc = pkgs.writeShellScript "rc" ''
            options=(
            	style=round
            	width=6.0
            	hidpi=off
            	active_color=0xffe2e2e3
            	inactive_color=0xff414550
            )

            borders "''${options[@]}"
          '';
        in
        "${rc}";
      target = "borders/bordersrc";
    };
  };
  launchd = {
    enable = true;
    agents.kitty-colorscheme-switcher = {
      enable = true;
      config =
        let
          run-or-notify = pkgs.writeShellScriptBin "run-or-notify" ''
            title="$1"
            shift
            output="$("$@" 2>&1)"
            err="$?"

            if [ "$err" -ne 0 ]; then
              osascript -e "display notification \"Output: $output\" with title \"$title\""
              exit $err
            fi
          '';
          theme-chooser = pkgs.writeShellScriptBin "theme-chooser" ''
            now="$(date +%H)"
            if [ "$now" -gt 8 ] && [ "$now" -lt 18 ]; then
              echo "Everforest Light Hard"
            else
              echo "Everforest Dark Hard"
            fi
          '';
        in
        {
          Program = "/Applications/kitty.app/Contents/MacOS/kitty";
          ProgramArguments = [
            "+kitten"
            "themes"
            "--reload-in"
            "all"
            "$(${theme-chooser}/bin/theme-chooser)"
          ];
          StartInterval = 3600;
        };
    };
  };
  myconfig = {
    mkHome = {
      enable = true;
      isNixOs = false;
      isPrivateDevice = false;
      isDarwin = true;
      useXfce = false;
      username = "jonas.weissensel";
      sessionVariables = {
        EDITOR = "hx";
        PAWS_MFA_AWS_USER = "joweisse";
      };
      sessionPath = [ "$HOME/.local/bin" ];
      stateVersion = myStateVersion;
      extraPackages = with pkgs; [
        # cli
        awscli2
        go-jira
        lazygit
        mob
        pre-commit
        ssm-session-manager-plugin
        terraform
        tokei
        colima
        docker

        # python
        uv

        # gui
        # (jetbrains.pycharm-professional.override {
        #   vmopts = ''
        #     -Duser.name=joweisse
        #   '';
        # })
        keepassxc
      ];
      programs = {
        fish.interactiveShellInit = ''
          if test -d /opt/homebrew
            set -gx HOMEBREW_PREFIX "/opt/homebrew";
            set -gx HOMEBREW_CELLAR "/opt/homebrew/Cellar";
            set -gx HOMEBREW_REPOSITORY "/opt/homebrew";
            fish_add_path -gP "/opt/homebrew/bin" "/opt/homebrew/sbin";
            ! set -q MANPATH; and set MANPATH ""; set -gx MANPATH "/opt/homebrew/share/man" $MANPATH;
            ! set -q INFOPATH; and set INFOPATH ""; set -gx INFOPATH "/opt/homebrew/share/info" $INFOPATH;
          end
          if uname | grep -q Darwin
            ssh-add --apple-load-keychain
          end
        '';
        fish.plugins = [
          {
            name = "bass";
            src = pkgs.fishPlugins.bass.src;
          }
          {
            name = "spark";
            src = pkgs.fetchFromGitHub {
              owner = "jorgebucaran";
              repo = "spark.fish";
              rev = "1.2.0";
              sha256 = "AIFj7lz+QnqXGMBCfLucVwoBR3dcT0sLNPrQxA5qTuU=";
            };
          }
          {
            name = "paws";
            src =
              let
                paws = pkgs.stdenv.mkDerivation {
                  pname = "paws";
                  version = "1";
                  src = builtins.fetchGit {
                    url = "ssh://git@github.com/otto-ec/seo_paws.git";
                    ref = "main";
                    rev = "51ed1df0c7ce22006c0dc576603d6f8b4fba23d4";
                  };
                  nativeBuildInputs = [ pkgs.jq ];
                  dontUnpack = true;
                  dontConfigure = true;
                  dontBuild = true;
                  installPhase = ''
                    mkdir -p $out/{completions,functions}
                    mkdir -p $out/opt
                    install -m 755 $src/paws/paws.sh $out/opt/paws.sh
                    install -m 755 $src/paws-fish/functions/paws.fish $out/functions/paws.fish
                    install -m 755 $src/paws-fish/completions/paws.fish $out/completions/paws.fish

                    patchShebangs --host $out/opt/paws.sh
                    substituteInPlace $out/functions/paws.fish --replace /opt $out/opt
                  '';
                };
              in
              "${paws}";
          }
        ];
        fd.enable = true;
        git = {
          userName = "Jonas Weissensel";
          userEmail = "Jonas.Weissensel@otto.de";
          signing = {
            key = "~/.ssh/id_ed25519";
            signByDefault = builtins.stringLength "~/.ssh/id_ed25519" > 0;
          };
        };
        poetry = {
          enable = true;
          settings = {
            virtualenvs = {
              create = true;
              in-project = true;
              prefer-active-python = true;
              options.no-pip = true;
              options.no-setuptools = true;
            };
          };
        };
        pyenv = {
          enable = false;
          enableFishIntegration = true;
          enableBashIntegration = true;
          enableZshIntegration = true;
        };
        zellij = {
          enable = true;
          enableFishIntegration = false;
          settings = {
            default_shell = "fish";
          };
        };
      };
    };
    programs = {
      nixvim = {
        enable = true;
        enableCopilot = true;
      };
      # zsh.enable = true;
    };
  };
}
