{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  myStateVersion,
  ...
}:
{
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    outputs.homeManagerModules.nixvim
    outputs.homeManagerModules.sakura
    outputs.homeManagerModules.mkHome
  ];
  xdg.configFile = {
    "plasma-workspace/env/path.sh" = {
      text = ''
        unset QT_IM_MODULE
        unset GTK_IM_MODULE
      '';
    };
  };
  myconfig = {
    mkHome = {
      enable = true;
      isNixOs = false;
      isPrivateDevice = false;
      useXfce = true;
      username = "joweisse";
      sessionVariables = {
        EDITOR = "hx";
        PAWS_MFA_AWS_USER = "joweisse";
        SSH_AUTH_SOCK = "/run/user/1000/ssh-agent";
        SSH_ASKPASS = "/usr/bin/ksshaskpass";
        SSH_ASKPASS_REQUIRE = "prefer";
        ZK_NOTEBOOK_DIR = "/home/joweisse/Documents/ZK";
      };
      sessionPath = [
        "$HOME/.local/bin"
        "$HOME/.local/share/flatpak/exports/share"
      ];
      stateVersion = myStateVersion;
      extraPackages = with pkgs; [
        # cli
        (writeShellScriptBin "ottovpn" ''
          export OPENSSL_CONF=/home/joweisse/Misc/ssl-paloalto-unsafe.conf
          /opt/paloaltonetworks/globalprotect/globalprotect launch-ui
        '')
        (writeShellScriptBin "start-sway" ''
          __dotfiles_wayland_teardown() {
            # true: as long as we try it's okay
            systemctl --user stop sway-session.target || true
            # this teardown makes it easier to switch between compositors
            unset DISPLAY SWAYSOCK WAYLAND_DISPLAY XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP XDG_SESSION_TYPE
            systemctl --user unset-environment DISPLAY SWAYSOCK WAYLAND_DISPLAY XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP XDG_SESSION_TYPE
            if command -v dbus-update-activation-environment >/dev/null; then
                dbus-update-activation-environment XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP XDG_SESSION_TYPE
            fi
          }
          __dotfiles_wayland_teardown
          # Session
          export XDG_SESSION_TYPE=wayland
          export XDG_SESSION_DESKTOP=sway
          export XDG_CURRENT_DESKTOP=sway
          # Set wayland related environment variables
          export MOZ_ENABLE_WAYLAND=1
          export QT_QPA_PLATFORM=wayland
          export SDL_VIDEODRIVER=wayland
          export _JAVA_AWT_WM_NONREPARENTING=1

          if command -v dbus-update-activation-environment >/dev/null; then
            dbus-update-activation-environment XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP XDG_SESSION_TYPE
          fi
          # without this, systemd starts xdg-desktop-portal without these environment variables,
          # and the xdg-desktop-portal does not start xdg-desktop-portal-wrl as expected
          # https://github.com/emersion/xdg-desktop-portal-wlr/issues/39#issuecomment-638752975
          systemctl --user import-environment XDG_CURRENT_DESKTOP XDG_SESSION_DESKTOP XDG_SESSION_TYPE
          systemd-run --quiet --unit=sway --user --wait sway --unsupported-gpu $@ || true
          __dotfiles_wayland_teardown
          #exec systemd-cat --identifier=sway sway "$@"
        '')
        awscli2
        go-jira
        lazygit
        mob
        pre-commit
        ssm-session-manager-plugin
        terraform
        tokei

        # python
        uv

        # gui
        google-chrome
        gtimelog
        jetbrains.pycharm-professional
        keepassxc
        sonata
      ];
      programs = {
        foot.enable = true;
        foot.server.enable = true;
        foot.settings.main.font = "BlexMono Nerd Font:size=13";
        foot.settings.main.shell = "/home/joweisse/.nix-profile/bin/fish";
        fish.plugins = [
          {
            name = "bass";
            src = pkgs.fishPlugins.bass.src;
          }
          {
            name = "spark";
            src = pkgs.fetchFromGitHub {
              owner = "jorgebucaran";
              repo = "spark.fish";
              rev = "1.2.0";
              sha256 = "AIFj7lz+QnqXGMBCfLucVwoBR3dcT0sLNPrQxA5qTuU=";
            };
          }
          {
            name = "paws";
            src =
              let
                paws = pkgs.stdenv.mkDerivation {
                  pname = "paws";
                  version = "1";
                  src = builtins.fetchGit {
                    url = "ssh://git@github.com/otto-ec/seo_paws.git";
                    ref = "main";
                    rev = "51ed1df0c7ce22006c0dc576603d6f8b4fba23d4";
                  };
                  nativeBuildInputs = [ pkgs.jq ];
                  dontUnpack = true;
                  dontConfigure = true;
                  dontBuild = true;
                  installPhase = ''
                    mkdir -p $out/{completions,functions}
                    mkdir -p $out/opt
                    install -m 755 $src/paws/paws.sh $out/opt/paws.sh
                    install -m 755 $src/paws-fish/functions/paws.fish $out/functions/paws.fish
                    install -m 755 $src/paws-fish/completions/paws.fish $out/completions/paws.fish

                    patchShebangs --host $out/opt/paws.sh
                    substituteInPlace $out/functions/paws.fish --replace /opt $out/opt
                  '';
                };
              in
              "${paws}";
          }
        ];
        freetube.enable = true;
        fd.enable = true;
        git = {
          userName = "Jonas Weissensel";
          userEmail = "Jonas.Weissensel@otto.de";
          signing = {
            key = "~/.ssh/id_ed25519";
            signByDefault = builtins.stringLength "~/.ssh/id_ed25519" > 0;
          };
        };
        i3status-rust.enable = true;
        poetry = {
          enable = true;
          settings = {
            virtualenvs = {
              create = true;
              in-project = true;
              prefer-active-python = true;
              options.no-pip = true;
              options.no-setuptools = true;
            };
          };
        };
        pyenv = {
          enable = true;
          enableFishIntegration = true;
          enableBashIntegration = true;
        };
        zellij = {
          enable = true;
          enableFishIntegration = false;
          settings = {
            default_shell = "fish";
          };
        };
      };
      services = {
        ssh-agent.enable = true;
      };
    };
    programs = {
      nixvim = {
        enable = true;
        enableCopilot = true;
      };
      sakura = {
        enable = true;
        settings = {
          font = "BlexMono Nerd Font 14";
          tabs_on_bottom = true;
        };
      };
    };
  };
}
