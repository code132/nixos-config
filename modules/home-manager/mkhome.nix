{
  inputs,
  outputs,
  config,
  lib,
  pkgs,
  myStateVersion,
  ...
}: let
  cfg = config.myconfig.mkHome;
  inherit (lib)
    mkEnableOption
    mkIf
    mkOption
    types
    ;
  mkHMOption =
    default:
    mkOption {
      inherit default;
      type = types.lazyAttrsOf types.anything;
      description = "Exposes the corresponding options from home-manager";
    };
in
{
  options.myconfig.mkHome = {
    enable = mkEnableOption "Enable a home-manager environment";
    isPrivateDevice = mkEnableOption "Enable this for your private devices";
    isDarwin = mkEnableOption "Enable this if the host runs on macOS";
    isNixOs = mkEnableOption "Enable this if the host runs NixOs";
    useKdePlasma = mkEnableOption "Enable this for default WM setup with kde plasma (wayland)";
    useGnome = mkEnableOption "Enable this for default WM setup with gnome 3 (wayland)";
    useXfce = mkEnableOption "Enable this for default WM setup with XFCE (X11)";
    useCustomWm = mkEnableOption "Enable this for completely custom WM setup";
    username = mkOption {
      type = types.str;
      description = "The username for the home-environment";
      example = ''"jonas"'';
    };
    sessionVariables = mkOption {
      type = types.attrsOf (
        types.oneOf [
          types.str
          types.int
          types.float
          types.bool
        ]
      );
      description = "Any environment variables available in the session for $USER";
      example = ''
        {
          EDITOR = "hx";
        }
      '';
    };
    sessionPath = mkOption {
      type = types.listOf types.str;
      description = "All paths that should be available in PATH";
      default = [];
    };
    extraPackages = mkOption {
      type = types.listOf types.package;
      description = "Any extra Packages that should be installed";
      default = [];
      example = ''
        [
          pkgs.firefox
        ]
      '';
    };
    stateVersion = mkOption {
      type = types.str;
      description = "The stateVersion of the NixOS/Home-manager installation (Do not change unless you know exactly why)";
      example = ''"23.11"'';
    };
    programs = mkHMOption {};
    services = mkHMOption {};
    systemd = mkHMOption {};
    xdg = mkHMOption {};
  };
  config = mkIf cfg.enable {
    home = {
      inherit (cfg) username sessionVariables stateVersion;
      homeDirectory = if cfg.isDarwin then "/Users/${cfg.username}" else "/home/${cfg.username}";
      packages =
        with pkgs;
        [
          # fonts
          nerd-fonts.blex-mono
          nerd-fonts.mplus
          nerd-fonts.terminess-ttf
          nerd-fonts.iosevka-term
          pkgs.font-awesome
          pkgs.cozette
          pkgs.fixedsys-excelsior
          # cli
          age-plugin-yubikey
          alejandra
          nixfmt
          niv
          eza
          fd
          github-cli
          magic-wormhole-rs
          mc
          nap
          nil
          nix-prefetch-git
          nh
          ripgrep
          wireguard-tools
          xclip
          xsel
          zk

          # gui
        ]
        ++ (
          if cfg.isDarwin then
            [ ]
          else
            [
              solaar
              signal-desktop
              standardnotes
              libreoffice
              gnome-secrets
              firefox
              bitwarden
              thunderbird
            ]
        )
        ++ cfg.extraPackages
        ++ (if cfg.useXfce then [ pkgs.xfce.mousepad ] else [ ]);
    };

    fonts = {
      fontconfig.enable = true;
    };
    gtk = mkIf cfg.useXfce {
      enable = true;
      iconTheme = {
        name = "elementary-Xfce-dark";
        package = pkgs.elementary-xfce-icon-theme;
      };
      theme = {
        name = "zukitre-dark";
        package = pkgs.zuki-themes;
      };
      gtk3.extraConfig = {
        gtk-application-prefer-dark-theme = 1;
      };
      gtk4.extraConfig = {
        gtk-application-prefer-dark-theme = 1;
      };
    };
    manual = {
      html.enable = true;
    };
    nixpkgs = {
      config = {
        allowUnfree = true;
        allowUnfreePredicate = _: true;
      };
    };
    programs =
      lib.recursiveUpdate {
        alacritty = mkIf cfg.isNixOs {
          enable = true;
          settings.general.import = let
            flavor = "mocha";
            catppucin = pkgs.fetchFromGitHub {
              owner = "catppuccin";
              repo = "alacritty";
              rev = "f2da554ee63690712274971dd9ce0217895f5ee0";
              sha256 = "sha256-ypYaxlsDjI++6YNcE+TxBSnlUXKKuAMmLQ4H74T/eLw=";
            };
          in
          [ "${catppucin}/catppuccin-${flavor}.toml" ];
        settings.font =
          let
            family = "BlexMono Nerd Font";
          in
          (lib.attrsets.genAttrs
            [
              "normal"
              "italic"
              "bold"
            ]
            (name: {
              inherit family;
            })
          )
          // {
            size = 13;
          };
        settings.keyboard.bindings = [
          {
            key = "N";
            mods = "Shift|Control";
            action = "CreateNewWindow";
          }
        ];
      };
      bash.enable = true;
      bat.enable = true;
      dircolors.enable = true;
      direnv = {
        enable = true;
        stdlib = ''
          # source: https://github.com/direnv/direnv/wiki/Customizing-cache-location#human-readable-directories
          : ''${XDG_CACHE_HOME:=$HOME/.cache}
          declare -A direnv_layout_dirs
          direnv_layout_dir() {
          	echo "''${direnv_layout_dirs[$PWD]:=$(
          		local hash="$(shasum -a 1 - <<<"''${PWD}" | cut -c-7)"
          		local path="''${PWD//[^a-zA-Z0-9]/-}"
          		echo "''${XDG_CACHE_HOME}/direnv/layouts/''${hash}''${path}"
          	)}"
          }
          layout_poetry() {
            if [[ ! -f pyproject.toml ]]; then
              log_error "No pyproject.toml found. Use `poetry new` or `poetry init` to create one first."
              exit 2
            fi

            local VENV=$(dirname $(poetry run which python))
            export VIRTUAL_ENV=$(echo "$VENV" | rev | cut -d'/' -f2- | rev)
            export POETRY_ACTIVE=1
            PATH_add "$VENV"
          }

        '';
        nix-direnv.enable = true;
      };
      fish = {
        enable = true;
        plugins = [
          {
            name = "spark";
            src = pkgs.fetchFromGitHub {
              owner = "jorgebucaran";
              repo = "spark.fish";
              rev = "1.2.0";
              sha256 = "AIFj7lz+QnqXGMBCfLucVwoBR3dcT0sLNPrQxA5qTuU=";
            };
          }
        ];
        functions = {
          ecr-login = {
            description = "Login to AWS ECR";
            body = ''
              fish --private --command "aws ecr get-login-password --profile $AWS_PROFILE --region eu-central-1 | docker login --username AWS --password-stdin $ACCOUNT_INFRASTRUCTURE.dkr.ecr.eu-central-1.amazonaws.com"
            '';
          };
          fancy_clear = {
            description = "A fancy terminal clearing";
            body = ''
              command clear
              seq 1 (tput cols) | sort --random-sort | spark | ${pkgs.lolcrab}/bin/lolcrab
              commandline -f repaint
            '';
          };
          fish_prompt = {
            body = ''
              set -l last_status $status

              set -l normal (set_color normal)
              set -l usercolor (set_color $fish_color_user)

              # set -l delim \U25BA
              set -l delim \UF0DA
              # If we don't have unicode use a simpler delimiter
              string match -qi "*.utf-8" -- $LANG $LC_CTYPE $LC_ALL; or set delim ">"

              # create VCS prompt
              set -g __fish_git_prompt_showdirtystate 1
              set -g __fish_git_prompt_color_branch_dirty yellow
              set -g __fish_git_prompt_color_branch_staged blue
              # set -g __fish_git_prompt_showuntrackedfiles 0
              set -g __fish_git_prompt_showupstream auto
              set -g __fish_git_prompt_shorten_branch_len 7
              set -g __fish_git_prompt_showcolorhints 1

              # set -g __fish_git_prompt_use_informative_chars 0
              # Unfortunately this only works if we have a sensible locale
              # string match -qi "*.utf-8" -- $LANG $LC_CTYPE $LC_ALL
              # and set -g __fish_git_prompt_char_dirtystate \U1F4a9

              set -g __fish_git_prompt_char_stateseparator ""
              set -g __fish_git_prompt_char_dirtystate ""
              set -g __fish_git_prompt_char_untrackedfiles ""
              set -g __fish_git_prompt_char_stagedstate ""
              set -g __fish_git_prompt_char_upstream_equal ""
              set -g __fish_git_prompt_char_upstream_ahead ↑
              set -g __fish_git_prompt_char_upstream_behind ↓

              set -l vcs (fish_vcs_prompt %s 2>/dev/null)

              # Prompt status only if it's not 0
              set -l prompt_status
              test $last_status -ne 0; and set prompt_status (set_color $fish_color_error)"[$last_status]$normal"

              # Only show host if in SSH or container
              # Store this in a global variable because it's slow and unchanging
              if not set -q prompt_host
                  set -g prompt_host ""
                  if set -q SSH_TTY
                      or begin
                          command -sq systemd-detect-virt
                          and systemd-detect-virt -q
                      end
                      set -l host (hostname)
                      set prompt_host $usercolor$USER$normal@(set_color $fish_color_host)$host$normal":"
                  end
              end

              set -l indicator (
                  if test -n "$IN_NIX_SHELL"
                    echo -n \Uf0e7
                  else
                    echo -n $delim
                  end
              )
              set -l local_aws_profile (
                if test -n "$AWS_PROFILE"
                  echo -n "[$AWS_PROFILE]@"
                end
              )

              echo -n -s $local_aws_profile$prompt_host $prompt_status $vcs $indicator
            '';
          };
          fish_right_prompt = {
            body = ''
                          set -l d (set_color brgrey)(date "+%R")(set_color normal)

              set -l duration "$cmd_duration$CMD_DURATION"
              if test $duration -gt 100
                  set duration (math $duration / 1000)s
              else
                  set duration
              end

              set -l normal (set_color normal)
              set -l usercolor (set_color $fish_color_user)
              set -l cwd (set_color $fish_color_cwd)
              if command -sq sha256sum
                  # randomized cwd color
                  set -l shas (pwd -P | sha256sum | string sub -l 6 | string match -ra ..)
                  # Increase color a bit so we don't get super dark colors.
                  # Really we want some contrast to the background (assuming black).
                  set -l col (for f in $shas; math --base=hex "min(255, 0x$f + 0x30)"; end | string replace 0x "" | string pad -c 0 -w 2 | string join "")

                  set cwd (set_color $col)
              end

              # Shorten pwd if prompt is too long
              set -l pwd (prompt_pwd)

              set -q VIRTUAL_ENV_DISABLE_PROMPT
              or set -g VIRTUAL_ENV_DISABLE_PROMPT true
              set -q VIRTUAL_ENV
              and set -l venv "py:"(string replace -r '.*/' "" -- "$VIRTUAL_ENV")

              set_color reset
              string join " " -- $venv $duration $cwd $pwd $normal $d
            '';
          };
          fish_greeting = "";
        };
        interactiveShellInit = ''
          bind \cL 'fancy_clear'
        '';
        shellAbbrs = {
          g = "git status";
          he = "";
          hs = "home-manager switch -b backup";
          ne = mkIf cfg.isNixOs "";
          ns = mkIf cfg.isNixOs "sudo nixos-rebuild switch";
          nt = mkIf cfg.isNixOs "sudo nixos-rebuild test";

          exa = "eza";
        };
      };
      fzf.enable = true;
      git = {
        enable = true;
        delta.enable = true;
        extraConfig = {
          init.defaultBranch = "main";
          gpg.format = "ssh";
          pull.rebase = true;
          rebase.autoStash = true;
        };
        ignores = [
          ".direnv"
          ".helix"
          ".vscode"
          "result"
        ];
      };
      gpg = {
        enable = true;
        scdaemonSettings = {
          disable-ccid = true;
        };
        settings = {
          # default-cache-ttl = "3600";
          # default-cache-ttl-ssh = "3600";
          # max-cache-ttl = "7200";
          # max-cache-ttl-ssh = "7200";
        };
      };
      helix = {
        enable = true;
        extraPackages = [
          pkgs.nixfmt
          pkgs.lazygit
          pkgs.marksman # for markdown
          pkgs.nodePackages.bash-language-server
          pkgs.ruff
          pkgs.shfmt
          pkgs.taplo # for toml
          pkgs.terraform-ls
          pkgs.zk
          (pkgs.python3.withPackages (ps: [
            ps.python-lsp-server
            ps.python-lsp-ruff
            ps.pylsp-rope
            ps.rope
            ps.ruff-lsp
          ]))
        ];

        languages = {
          language-server.zk = {
            command = "zk";
            args = [ "lsp" ];
          };
          language = [
            {
              name = "fish";
              formatter = {
                command = "fish_indent";
              };
              auto-format = true;
            }
            {
              name = "nix";
              auto-format = true;
              formatter = {
                command = "nixfmt";
              };
            }
            {
              name = "markdown";
              file-types = [
                "md"
                "markdown"
              ];
              injection-regex = "md|markdown";
              # roots = [".zk"];
              language-servers = [
                # {name = "zk";}
                { name = "marksman"; }
              ];
            }
            {
              name = "bash";
              auto-format = true;
              indent = {
                tab-width = 2;
                unit = "  ";
              };
              formatter = {
                command = "${pkgs.shfmt}/bin/shfmt";
                args = [
                  "-i"
                  "2"
                ];
              };
            }
            {
              name = "python";
              auto-format = true;
              language-servers = [
                {
                  name = "ruff-lsp";
                  only-features = [
                    "format"
                    "diagnostics"
                  ];
                }
                { name = "pylsp"; }
              ];
            }
          ];
        };
        settings = {
          theme = "catppuccin_mocha";
          editor = {
            bufferline = "multiple";
            idle-timeout = 0;
            file-picker.hidden = false; # ignore hidden files: false
            lsp.display-messages = true;
            lsp.display-inlay-hints = true;
            statusline = {
              left = [
                "mode"
                "spinner"
              ];
              center = [ "file-name" ];
              right = [
                "diagnostics"
                "selections"
                "position"
                "version-control"
                "file-encoding"
                "file-line-ending"
                "file-type"
              ];
            };
          };
          keys.normal = {
            space.space = ":reload-all";
            ";".";" = "command_mode";
            ";".w = ":w";
            ";".W = ":wa";
            ";".q = ":q";
            ";".Q = ":qa";
            ";".x = ":wq";
            ";".X = ":wq";
            g.q = ":reflow";
            "C-g" = '':sh tmux popup -d "#{pane_current_path}" -xC -yC -w80% -h80% -E lazygit'';
          };
        };
      };
      home-manager.enable = true;
      jq.enable = true;
      eza = {
        enable = true;
        git = true;
        icons = true;
      };
      mpv = mkIf cfg.isPrivateDevice {
        enable = true;
        scripts = [ pkgs.mpvScripts.mpris ];
      };
      nheko = mkIf cfg.isPrivateDevice { enable = false; };
      tmux = {
        enable = true;
        baseIndex = 1;
        clock24 = true;
        escapeTime = 15;
        historyLimit = 100000;
        mouse = true;
        newSession = false;
        shell = "${pkgs.fish}/bin/fish";
        terminal = "tmux-256color";
        plugins = with pkgs.tmuxPlugins; [
          better-mouse-mode
          extrakto
          catppuccin
          tmux-fzf
        ];
        extraConfig = ''
          bind r source-file $HOME/.config/tmux/tmux.conf; display "Reloaded tmux.conf"
          set -g @catppuccin_date_time "%Y-%m-%d %H:%M"
        '';
      };
      vscode = {
        enable = true;
        package = pkgs.vscodium;
        mutableExtensionsDir = false;
        extensions = with pkgs.vscode-extensions; [
          arrterian.nix-env-selector
          bbenoist.nix
          editorconfig.editorconfig
          jnoortheen.nix-ide
          kamadorueda.alejandra
        ];
      };
      zoxide = {
        enable = true;
        enableBashIntegration = true;
        enableFishIntegration = true;
      };
      zathura.enable = true;
    } cfg.programs;

    services = lib.recursiveUpdate {
      gnome-keyring = mkIf cfg.isPrivateDevice {
        enable = true;
        components = [
          "pkcs11"
          "secrets"
          "ssh"
        ];
      };
      gpg-agent = mkIf cfg.isPrivateDevice { enable = true; };
      # network-manager-applet = mkIf cfg.isPrivateDevice { enable = true; };
      # udiskie = mkIf cfg.isPrivateDevice {
      #   enable = true;
      #   # tray = "always";
      # };
      copyq = mkIf cfg.isPrivateDevice { enable = true; };
      mpd = mkIf cfg.isPrivateDevice {
        enable = true;
        network.startWhenNeeded = true;
      };
    } cfg.services;
    systemd = lib.recursiveUpdate {
      user = {
        # Nicely reload system units when changing configs
        startServices = "sd-switch";
      };
    } cfg.systemd;
    targets.genericLinux.enable = !cfg.isNixOs && !cfg.isDarwin;
    xdg = lib.recursiveUpdate {
      userDirs = mkIf cfg.isPrivateDevice {
        enable = true;
        createDirectories = true;
      };
      configFile = mkIf cfg.isPrivateDevice {
        zk-config =
          let
            config = pkgs.writeText "zk-config" ''
              [notebook]
              dir = "~/Documents/ZK"

              [note]
              language = "de"
              filename = "{{id}}-{{slug title}}"
              extension = "md"

              [extra]
              author = "Jonas"

              [format.markdown]
              # Enable support for #hashtags
              hashtags = true
              # Enable support for :colon:separated:tags:
              colon-tags = true

              [tool]
              editor = "hx"

              [filter]
              recents = "--sort created- --created-after 'last two weeks'"

              [alias]
              edlast = "zk edit --limit 1 --sort modified- $@"

              [lsp]
              [lsp.diagnostics]
              wiki-title = "hint"
              dead-link = "error"

            '';
          in
          {
            source = "${config}";
            target = "zk/config.toml";
          };
        fish-catppuccin =
          let
            src = pkgs.fetchFromGitHub {
              owner = "catppuccin";
              repo = "fish";
              rev = "0ce27b518e8ead555dec34dd8be3df5bd75cff8e";
              sha256 = "sha256-Dc/zdxfzAUM5NX8PxzfljRbYvO9f9syuLO8yBr+R3qg=";
            };
          in
          {
            source = "${src}/themes/Catppuccin Mocha.theme";
            target = "fish/themes/Catppuccin Mocha.theme";
          };
      };
    } cfg.xdg;
  };
}
