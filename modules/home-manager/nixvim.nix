{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.myconfig.programs.nixvim;
in {
  options.myconfig.programs.nixvim = {
    enable = lib.mkEnableOption "Enable nixvim";
    enableCopilot = lib.mkEnableOption "Enable Github Copilot";
  };
  config = lib.mkIf cfg.enable {
    programs.nixvim = {
      enable = true;
      colorschemes = {
        catppuccin.enable = true;
        catppuccin.settings.flavour = "mocha";
        catppuccin.settings.dim_inactive.enabled = true;
      };
      extraConfigLua = ''
        vim.diagnostic.config({
          virtual_text = false,
        })
        -- function necessary for nvim-cmp for <Tab> completion with snippets
        has_words_before = function()
          unpack = unpack or table.unpack
          local line, col = unpack(vim.api.nvim_win_get_cursor(0))
          return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
        end
      '';
      globals = {mapleader = " ";};
      opts = {
        conceallevel = 2;
        # unfold file by default
        foldlevel = 99;
        number = true;
        relativenumber = true;
        scrolloff = 8;
        shiftwidth = 2;
        timeoutlen = 20;
        ttimeoutlen = 5;
      };
      extraPackages = with pkgs; [
        alejandra
        luaPackages.luacheck
        nixfmt
        pandoc
        ruff
        ruff-lsp
        python3Packages.mypy
        python3Packages.mdformat-footnote
        python3Packages.mdformat-frontmatter
        python3Packages.mdformat-gfm
        python3Packages.mdformat-nix-alejandra
        ripgrep
        shfmt
        tflint
        tfsec
        fd
      ];
      clipboard.providers.xclip.enable = true;
      keymaps =
        let
          to_leader_keymap =
            {
              key,
              action,
              desc ? "",
            }:
            {
              inherit action;
              key = "<leader>${key}";
              options.silent = true;
              options.desc = desc;
              mode = "n";
            };
        in
        [
          # general
          (to_leader_keymap {
            key = "<leader>";
            action = ":e!<CR>";
            desc = "Reload file";
          })
          (to_leader_keymap {
            key = "rr";
            action = ":source $MYVIMRC<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "<leader>e";
            action = ":lua vim.diagnostic.open_float()";
            desc = "Open diagnostics";
          })
          # comments => c
          # (to_leader_keymap {key="cc" ; action="gcc"; desc=""})
          # status => s
          (to_leader_keymap {
            key = "ss";
            action = ":w<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "sw";
            action = ":w<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "sW";
            action = ":wa<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "sq";
            action = ":q<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "sQ";
            action = ":qa<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "sx";
            action = ":x<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "sX";
            action = ":xa<CR>";
            desc = "";
          })
          # find => f
          (to_leader_keymap {
            key = "ff";
            action = "<cmd>Telescope fd<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "fg";
            action = "<cmd>Telescope git_files<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "fl";
            action = "<cmd>Telescope live_grep<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "fb";
            action = "<cmd>Telescope buffers<CR>";
            desc = "";
          })
          # lsp => l
          (to_leader_keymap {
            key = "ld";
            action = "<cmd>Telescope lsp_definitions<CR>";
            desc = "Find or go to definitions";
          })
          (to_leader_keymap {
            key = "li";
            action = "<cmd>Telescope lsp_implementations<CR>";
            desc = "Find or go to implementations";
          })
          (to_leader_keymap {
            key = "lr";
            action = "<cmd>Telescope lsp_references<CR>";
            desc = "Find or go to references";
          })
          (to_leader_keymap {
            key = "ls";
            action = "<cmd>Telescope lsp_document_symbols<CR>";
            desc = "Find symbol in buffer";
          })
          (to_leader_keymap {
            key = "lw";
            action = "<cmd>Telescope lsp_workspace_symbols<CR>";
            desc = "Find symbol in workspace";
          })
          # window => w
          (to_leader_keymap {
            key = "ww";
            action = "<C-w>w";
            desc = "Cycle windows";
          })
          (to_leader_keymap {
            key = "wW";
            action = "<C-w>W";
            desc = "Cycle windows backwards";
          })
          (to_leader_keymap {
            key = "wc";
            action = "<C-w>c";
            desc = "Close window";
          })
          (to_leader_keymap {
            key = "ws";
            action = "<C-w>s";
            desc = "Horizontal split";
          })
          (to_leader_keymap {
            key = "wv";
            action = "<C-w>v";
            desc = "Vertical split";
          })
          # Git => G
          (to_leader_keymap {
            key = "Gn";
            action = "<cmd>Neogit kind=split<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "Gms";
            action = ":!mob start<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "Gmn";
            action = ":!mob next<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "Gmd";
            action = ":!mob done<CR>";
            desc = "";
          })
          # tools => t
          (to_leader_keymap {
            key = "td";
            action = "<cmd>TroubleToggle<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "tt";
            action = "<cmd>ToggleTerm<CR>";
            desc = "";
          })
          # notes => n
          (to_leader_keymap {
            key = "nf";
            action = "<cmd>ZkNotes<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "nn";
            action = "<cmd>ZkNew {title = vim.fn.input('Title: ')}<CR>";
            desc = "";
          })
          (to_leader_keymap {
            key = "nd";
            action = "<cmd>ZkNew {dir = 'journal/daily', date = 'today', tags = {'daily'}}<CR>";
            desc = "";
          })
          # Formatting in Markdown => h
          (to_leader_keymap {
            key = "hhi";
            action = "<cmd>HeaderIncrease<CR>";
            desc = "Increase all headers in buffer";
          })
          (to_leader_keymap {
            key = "hhd";
            action = "<cmd>HeaderDecrease<CR>";
            desc = "Decrease all headers in buffer";
          })
          (to_leader_keymap {
            key = "hft";
            action = "<cmd>TableFormat<CR>";
            desc = "Format table under cursor";
          })
        ];
      extraPlugins = with pkgs.vimPlugins; [
        friendly-snippets
        nvim-web-devicons
        vim-markdown
        tabular
      ];
      plugins = {
        alpha = {
          enable = true;
          theme = "dashboard";
        };
        cmp = {
          enable = true;
          settings.mapping = {
            "<CR>" = "cmp.mapping.confirm({ select = false })";
            "<Tab>" = ''
              cmp.mapping(function(fallback)
                if cmp.visible() then
                  cmp.select_next_item()
                elseif require("luasnip").expand_or_locally_jumpable() then
                  require("luasnip").expand_or_jump()
                elseif has_words_before() then
                  cmp.complete()
                else
                  fallback()
                end
              end, { "i", "s" })
            '';
            "<S-Tab>" = ''
              cmp.mapping(function(fallback)
                if cmp.visible() then
                  cmp.select_prev_item()
                elseif require("luasnip").jumpable(-1) then
                  require("luasnip").jump(-1)
                else
                  fallback()
                end
              end, { "i", "s" })
            '';
          };
          settings.snippet.expand = "function(args) require('luasnip').lsp_expand(args.body) end";
          settings.sources = [
            # {
            #   # only enable this to have copilot in autocomplete popup
            #   name = "copilot";
            #   groupIndex = 1;
            # }
            {
              name = "nvim_lsp";
              groupIndex = 1;
            }
            {
              name = "nvim_lsp_signature_help";
              groupIndex = 1;
            }
            {
              name = "treesitter";
              groupIndex = 1;
            }
            {
              name = "luasnip";
              groupIndex = 1;
            }
            {
              name = "path";
              groupIndex = 2;
            }
            {
              name = "buffer";
              groupIndex = 2;
            }
            # {
            #   name = "digraphs";
            #   groupIndex = 3;
            # }
          ];
        };
        cmp_luasnip.enable = true;
        comment = {
          enable = true;
          settings.padding = true;
        };
        conform-nvim = {
          enable = true;
          settings = {
            format_on_save = {
              lspFallback = true;
              timeoutMs = 500;
            };
            format_after_save = {lspFallback = true;};
            formatters_by_ft = {
              bash = ["shfmt"];
              fish = ["fish_indent"];
              nix = ["nixfmt"];
              python = ["ruff_fix" "ruff_format"];
              terraform = ["terraform_fmt"];
            };
          };
        };
        # copilot-cmp.enable = false;  # conflicts with copilot-lua
        copilot-lua = lib.mkIf cfg.enableCopilot {
          enable = true;
          # panel.enabled = false;
          suggestion.enabled = true;
          suggestion.autoTrigger = true;
          suggestion.keymap.acceptWord = "<M-f>";
          filetypes.markdown = true;
        };
        dap = {
          enable = false;
          extensions.dap-python = {
            enable = false;
            testRunner = "pytest";
            adapterPythonPath = "${pkgs.python3.withPackages (ps: [ps.debugpy])}/bin/python";
          };
        };
        diffview.enable = true;
        flash.enable = true;
        lint = {
          enable = true;
          lintersByFt = {
            python = ["ruff" "mypy"];
            fish = ["fish"];
            lua = ["luacheck"];
            nix = ["nix"];
            terraform = ["tfsec"];
          };
        };
        lsp = {
          enable = true;
          keymaps.lspBuf = {
            K = "hover";
            # gr = "references";
            # gd = "definition";
            # gi = "implementation";
            # gt = "type_definition";
          };
          servers = {
            bashls.enable = true;
            jsonls.enable = true;
            lua_ls.enable = true;
            nil_ls.enable = true;
            pylsp.enable = true;
            pylsp.settings.plugins.ruff.enabled = true;
            pylsp.settings.plugins.ruff.lineLength = 150;
            # ruff-lsp.enable = false;
            terraformls.enable = true;
            yamlls.enable = true;
            # zls.enable = true;
          };
        };
        # lsp-lines.enable = true;
        lualine.enable = true;
        luasnip = {
          enable = true;
          settings = {
            enable_autosnippets = true;
            store_selection_keys = "<Tab>";
          };
          fromVscode = [{}];
        };
        gitsigns.enable = true;
        markdown-preview = {
          enable = true;
          settings.echo_preview_url = 1;
        };
        neogit.enable = true;
        nix.enable = true;
        project-nvim.enable = true;
        project-nvim.enableTelescope = true;
        oil.enable = true;
        vim-surround.enable = true;
        telescope = {
          enable = true;
          # extensions.file_browser = {
          #   enable = true;
          #   hidden = true;
          # };
          keymaps = {};
        };
        todo-comments.enable = true;
        treesitter = {
          enable = true;
          folding = true;
        };
        treesitter-refactor = {
          enable = true;
          highlightDefinitions.enable = true;
          smartRename.enable = true;
          navigation = {
            enable = true;
            keymaps.gotoDefinition = null;
            keymaps.gotoDefinitionLspFallback = "gnd";
          };
        };
        treesitter-context = {
          settings = {
            separator = "-";
            min_window_height = 30;
            max_lines = 3;
          };
          enable = true;
        };
        toggleterm = {
          enable = true;
          settings.direction = "float";
        };
        trouble.enable = true;
        vim-matchup.enable = true;
        web-devicons.enable = true;
        which-key = {
          enable = true;
          plugins.presets.operators = false;
          operators = {
            "gc" = "Comments";
          };
          registrations = {
            # "<leader>ff" = "Find files via Telescope (fd)";
            # {
            #   name = "Leader";
            #   c = "Comments";
            #   f = "Find";
            #   g = "Git";
            #   h = "Help";
            #   l = "LSP";
            #   p = "Project";
            #   t = "Treesitter";
            #   w = "Windows";
            # };
            key_labels = {
              "<leader>" = "SPC";
              "<cr>" = "RET";
            };
            layout.height = {
              min = 1;
              max = 7;
            };
            plugins.presets.operators = false;
            operators = {"gc" = "Comments";};
          };
        };
        zk = {
          enable = true;
          settings.picker = "telescope";
        };
      };
    };
  };
}
