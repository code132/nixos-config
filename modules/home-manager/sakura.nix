{
  lib,
  config,
  pkgs,
  ...
}:
let
  cfg = config.myconfig.programs.sakura;
  yesNoType = lib.types.enum [
    "Yes"
    "No"
  ];
  renderValue =
    option:
    {
      int = builtins.toString option;
      bool = if option then "true" else "false";
      string = option;
      enum = option;
    }
    .${builtins.typeOf option};
in
{
  options.myconfig.programs.sakura = with lib; {
    enable = mkEnableOption "Enable sakura";
    settings = {
      font = mkOption {
        type = types.str;
        default = "DejaVu Sans Mono 12";
        description = "The font to use in sakura. Can have multiple fonts separated by comma.";
      };
      show_always_first_tab = mkOption {
        type = yesNoType;
        default = "No";
        description = "Always show the first tab";
      };
      scrollbar = mkOption {
        type = types.bool;
        default = true;
        description = "Show scrollbar";
      };
      resize_grip = mkOption {
        type = types.bool;
        default = false;
        description = "Show resize grip";
      };
      closebutton = mkOption {
        type = types.bool;
        default = true;
        description = "Show close button";
      };
      tabs_on_bottom = mkOption {
        type = types.bool;
        default = false;
        description = "Show tabs on bottom";
      };
      less_questions = mkOption {
        type = types.bool;
        default = false;
        description = "Less questions";
      };
      audible_bell = mkOption {
        type = yesNoType;
        default = "Yes";
        description = "Enable Audible bell";
        # audible_bell=Yes
      };
      visible_bell = mkOption {
        type = yesNoType;
        default = "No";
        description = "Enable Visible bell";
        # visible_bell=No
      };
      urgent_bell = mkOption {
        type = yesNoType;
        default = "Yes";
        description = "Enable Urgent bell";
        # urgent_bell=Yes
      };
      blinking_cursor = mkOption {
        type = yesNoType;
        default = "No";
        description = "Enable Blinking cursor";
        # blinking_cursor=No
      };
      scroll_lines = mkOption {
        type = types.int;
        default = 4096;
        description = "Scroll lines to show";
      };
      # background=none
      # cursor_type=VTE_CURSOR_SHAPE_BLOCK
      # word_chars=-A-Za-z0-9,./?%&#_~
      # palette=xterm
      # add_tab_accelerator=5
      # del_tab_accelerator=5
      # switch_tab_accelerator=8
      # move_tab_accelerator=5
      # copy_accelerator=5
      # scrollbar_accelerator=5
      # open_url_accelerator=5
      # font_size_accelerator=4
      # set_tab_name_accelerator=5
      # add_tab_key=T
      # del_tab_key=W
      # prev_tab_key=Left
      # next_tab_key=Right
      # copy_key=C
      # paste_key=V
      # scrollbar_key=S
      # set_tab_name_key=N
      # fullscreen_key=F11
      # set_colorset_accelerator=5
      # icon_file=terminal-tango.svg
      # colorset1_fore=rgb(192,192,192)
      # colorset1_back=rgb(0,0,0)
      # colorset1_curs=rgb(255,255,255)
      # colorset1_key=F1
      # colorset2_fore=rgb(192,192,192)
      # colorset2_back=rgb(0,0,0)
      # colorset2_curs=rgb(255,255,255)
      # colorset2_key=F2
      # colorset3_fore=rgb(192,192,192)
      # colorset3_back=rgb(0,0,0)
      # colorset3_curs=rgb(255,255,255)
      # colorset3_key=F3
      # colorset4_fore=rgb(192,192,192)
      # colorset4_back=rgb(0,0,0)
      # colorset4_curs=rgb(255,255,255)
      # colorset4_key=F4
      # colorset5_fore=rgb(192,192,192)
      # colorset5_back=rgb(0,0,0)
      # colorset5_curs=rgb(255,255,255)
      # colorset5_key=F5
      # colorset6_fore=rgb(192,192,192)
      # colorset6_back=rgb(0,0,0)
      # colorset6_curs=rgb(255,255,255)
      # colorset6_key=F6
      # last_colorset=4
      # disable_numbered_tabswitch=false
      # allow_bold=Yes
      # increase_font_size_key=plus
      # decrease_font_size_key=minus
      # use_fading=false
      # scrollable_tabs=true
      # stop_tab_cycling_at_end_tabs=No
      # search_accelerator=5
      # search_key=F
    };
  };
  config = lib.mkIf cfg.enable {
    home.packages = [ pkgs.sakura ];
    xdg.configFile."sakura/sakura.conf".text = ''
      [sakura]
      ${lib.concatStringsSep "\n" (
        lib.mapAttrsToList (name: value: "${name}=${renderValue value}") cfg.settings
      )}
    '';
  };
}
