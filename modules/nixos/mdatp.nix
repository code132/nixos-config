{
  lib,
  pkgs,
  config,
  ...
}:
let
  cfg = config.programs.mdatp;
in
{
  options.programs.mdatp = {
    enable = lib.mkEnableOption "Enable the MS Defender for Endpoint app";
    package = lib.mkPackageOption pkgs "mdatp" {
      # default = self.packages.${pkgs.system}.mdatp;
      default = null;
      example = "self.packages.x86_64-linux.mdatp";
    };
  };
  config = lib.mkIf cfg.enable {
    boot.kernelParams = [ "fanotify" ];
    security.audit.enable = true;
    security.audit.rules = [
      # Watch /etc/passwd for modifications and tag with 'passwd'
      "-w /etc/passwd -p wa -k passwd"
    ];
    security.auditd.enable = true;
    # programs.nix-ld.enable = true;
    # environment.variables = {
    #   NIX_LD_LIBRARY_PATH = lib.mkDefault (with pkgs;
    #     lib.makeLibraryPath [
    #       systemd
    #       libselinux
    #       glibc
    #       audit
    #       acl
    #       zlib
    #       glib
    #       libnfnetlink
    #       libelf
    #       lttng-ust
    #       lttng-tools
    #       policycoreutils
    #       libglibutil
    #       libnetfilter_queue

    #       stdenv.cc.cc
    #       openssl
    #       libva
    #       libelf

    #       # Required
    #       glib
    #       gtk2
    #       bzip2

    #       # Without these it silently fails
    #       nspr
    #       nss
    #       cups
    #       libcap
    #       SDL2
    #       libusb1
    #       dbus-glib
    #       ffmpeg
    #       # Only libraries are needed from those two
    #       libudev0-shim

    #       # Other things from runtime
    #       flac
    #       freeglut
    #       libjpeg
    #       libpng
    #       libpng12
    #       libsamplerate
    #       libmikmod
    #       libtheora
    #       libtiff
    #       pixman
    #       speex
    #       SDL_image
    #       SDL_ttf
    #       SDL_mixer
    #       SDL2_ttf
    #       SDL2_mixer
    #       libappindicator-gtk2
    #       libdbusmenu-gtk2
    #       libindicator-gtk2
    #       libcaca
    #       libcanberra
    #       libgcrypt
    #       libvpx
    #       librsvg
    #       xorg.libXft
    #       libvdpau
    #       gnome2.pango
    #       cairo
    #       atk
    #       gdk-pixbuf
    #       fontconfig
    #       freetype
    #       dbus
    #       alsaLib
    #       expat
    #     ]);
    #   NIX_LD = lib.mkDefault (lib.fileContents "${pkgs.stdenv.cc}/nix-support/dynamic-linker");
    # };
    environment = {
      systemPackages = [ cfg.package ];
      pathsToLink = [
        "/usr/bin"
        "/opt/microsoft/mdatp"
      ];
      etc = {
        "audisp/plugins.d/syslog.conf" = {
          text = ''
            active = no
          '';
        };
        "auditd.conf" = lib.mkDefault {
          text = ''
            plugin_dir = /etc/audit/plugins.d
          '';
        };
        "audit/plugins.d/mdatp_audisp_plugin.conf" = {
          text = ''
            active = yes
            path = /etc/audit/plugins.d/mdatp_audisp_plugin
            args = -d
            format = string
          '';
        };
        "mdatp_audisp_plugin" = {
          source = "${cfg.package}/opt/microsoft/mdatp/sbin/mdatp_audisp_plugin";
          target = "audit/plugins.d/mdatp_audisp_plugin";
          mode = "0750";
        };
        "mdatp_onboard.json" = {
          source = "${cfg.package}/etc/opt/microsoft/mdatp/mdatp_onboard.json";
          target = "opt/microsoft/mdatp/mdatp_onboard.json";
        };
        "mdatp_managed.json" = {
          text = builtins.toJSON {
            features = {
              ebpfSupplementaryEventProvider = "enabled";
            };
            edr = {
              tags = [
                {
                  key = "GROUP";
                  "value" = "Unmanaged_OTTO";
                }
              ];
            };
          };
          target = "opt/microsoft/mdatp/managed/mdatp_managed.json";
        };
      };
    };

    users.users.mdatp = {
      isSystemUser = true;
      createHome = false;
      shell = null;
      description = "User for Microsoft Endpoint Security (mdatp)";
      group = "mdatp";
    };
    users.groups.mdatp = { };
    systemd.timers.mde_autoupdate = {
      description = "Timer to Trigger Weekly Updates for Microsoft Defender for Endpoint";
      wantedBy = [ "multi-user.target" ];
      timerConfig = {
        OnCalendar = "daily";
        AccuracySec = "1us";
        RandomizedDelaySec = "180min";
        Persistent = true;
        Unit = "mde_autoupdate.service";
      };
    };
    systemd.services.mde_autoupdate = {
      enable = true;
      description = "Service to update Microsoft Defender";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStartPre = "-pkill -f ${cfg.package}/opt/microsoft/mdatp/sbin/update_executor";
        ExecStart = "${cfg.package}/opt/microsoft/mdatp/sbin/update_executor";
        User = "root";
        Group = "root";
        PrivateTmp = true;
        NoNewPrivileges = true;
      };
    };
    systemd.services.mdatp = {
      enable = true;
      description = "Microsoft Defender";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      environment = {
        # LD_LIBRARY_PATH = "${cfg.package}/opt/microsoft/mdatp/lib";
        MALLOC_ARENA_MAX = "2";
        ENABLE_CRASHPAD = "1";
      };
      unitConfig = {
        StartLimitInterval = 120;
      };
      serviceConfig = {
        Type = "simple";
        # WorkingDirectory = "/var/tmp/microsoft/mdatp";
        LimitNOFILE = 65536;
        ExecStart = "${cfg.package}/opt/microsoft/mdatp/sbin/wdavdaemon";
        NotifyAccess = "main";
        Restart = "always";
        StartLimitBurst = 3;
        DELEGATE = "yes";
      };
    };
  };
}
