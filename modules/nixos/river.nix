{
  lib,
  pkgs,
  config,
  ...
}:
with lib;
let
  cfg = config.jonas.wayland;
in
{
  options = {
    cfg.enable = mkEnableOption "Enable Wayland";
  };
  config = mkIf cfg.enable {
    environment.sessionVariables = {
      MOZ_ENABLE_WAYLAND = 1;
    };
    environment.systemPackages =
      let
        waylandLock = pkgs.writeShellApplication {
          name = "wayland-lock";
          runtimeInputs = [ pkgs.swaylock ];
          text = ''
            [[ ! -d ''${HOME}/.config/lock ]] && mkdir -p ''${HOME}/.config/lock
            MUTEX="''${HOME}/.config/lock/river-lock"
            LOG="''${HOME}/.config/lock/lock-log"
            NOLOCK="''${HOME}/.config/lock/NOLOCK"

            img=$(find "''${HOME}"/.config/river/backgrounds -type f | shuf -n 1)

            VERSION="0.9"

            log () {
            	when=$(date "+%Y-%m-%d %H:%M:%S")
            	msg="[lock ''${VERSION}] ''${when} $1"
            	echo "''${msg}" >> "''${LOG}"
            }

            lock () {
            	if [ ! -f "''${NOLOCK}" ]; then
            		swaylock -F -l -i "$img"
            	else
            		log "''${NOLOCK} found, not locking"
            	fi
            }

            if [ "$1" = force ]; then
            	log "Forcing lock, removing ''${NOLOCK} and ''${MUTEX}"
            	rm -rf "''${NOLOCK}"
            	rm -rf "''${MUTEX}"
            fi

            if mkdir "$MUTEX"; then
            	log "Successfully acquired lock"

            	trap 'rm -rf "$MUTEX"' 0	# remove mutex when script finishes

            	lock
            else
            	log "cannot acquire lock, giving up on $MUTEX"
            	exit 0
            fi;
          '';
        };
        waylandLeave = pkgs.writeShellScriptBin "wayland-leave" ''
          choice=$(printf "Lock\nLogout\nSuspend\nReboot\nShutdown" | rofi -dmenu)
          if [[ $choice == "Lock" ]];then
            ${waylandLock}/bin/wayland-lock lock
          elif [[ $choice == "Logout" ]];then
              pkill -KILL -u "''${USER}"
          elif [[ $choice == "Suspend" ]];then
              systemctl suspend
          elif [[ $choice == "Reboot" ]];then
              systemctl reboot
          elif [[ $choice == "Shutdown" ]];then
              systemctl poweroff
          fi
        '';
      in
      [
        waylandLeave
        waylandLock
      ];
    programs.river.enable = true;
    programs.river.extraPackages =
      with pkgs;
      let
        riverAutostart = pkgs.writeShellApplication {
          name = "river-autostart";
          runtimeInputs = [
            killall
            waybar
          ];
          text = ''
            killall -q waybar
            waybar -c ~/.config/waybar/config -s ~/.config/waybar/style.css
          '';
        };
      in
      [
        rivercarro
        swaylock
        foot
        dmenu
        rofi
        riverAutostart
      ];
  };
}
