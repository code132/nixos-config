{
  lib,
  stdenv,
  fetchurl,
}:
stdenv.mkDerivation rec {
  pname = "wchisp-udev-rules";
  version = "1.0.0";

  udevRules = fetchurl {
    # pinned to a commit that works
    url = "https://raw.githubusercontent.com/jmaselbas/wch-isp/f38ae69c505acab5a71115df16db0b33085b3f44/50-wchisp.rules";
    sha256 = "0syx1rwxqg53v63lwx1iw3shbnrzxrpfm7fpdaaqlqwa74asav40";
  };

  dontUnpack = true;

  installPhase = ''
    cp ${udevRules} 50-wchisp.rules
    mkdir -p $out/lib/udev/rules.d
    cp 50-wchisp.rules $out/lib/udev/rules.d/50-wchisp.rules
  '';

  meta = {
    description = "Udev rules for ch552t microcontrollers flashed via wchisp (like Miao)";
    license = lib.licenses.gpl2;
    maintainers = with lib.maintainers; [ joweiss ];
    platforms = lib.platforms.linux;
    homepage = "https://github.com/jmaselbas/wch-isp/tree/master/50-wchisp.rules";
  };
}
