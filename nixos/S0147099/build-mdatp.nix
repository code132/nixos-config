{
  fetchurl,
  stdenv,
  dpkg,
  makeWrapper,
  systemd,
  libselinux,
  autoPatchelfHook,
  glibc,
  audit,
  acl,
  elfutils,
  libnetfilter_queue,
  zlib,
  glib,
  libglibutil,
  policycoreutils,
  libnfnetlink,
  libelf,
  requireFile,
}:
let
  license = stdenv.mkDerivation rec {
    pname = "mdatp_onboard.json";
    version = "0";

    src = requireFile {
      name = pname;
      url = /nix/store/qfvprsmkfh36fpwyy2a9bas1fjr9gipv-mdatp_onboard.json;
      sha256 = "0as89q69jj7v7lyzp418qqhl1d9yb3ngw7mwbp4wahs944n263ax";
    };

    dontUnpack = true;
    dontMakeSourcesWritable = true;
    dontPatch = true;
    dontConfigure = true;
    dontBuild = true;

    installPhase = ''
      mkdir -p $out
      cp $src $out/mdatp_onboard.json
    '';
  };
in
stdenv.mkDerivation rec {
  pname = "mdatp";
  version = "101.23102.0003";
  src = fetchurl {
    url = "https://packages.microsoft.com/debian/12/prod/pool/main/m/${pname}/${pname}_${version}_amd64.deb";
    sha256 = "079s59l241d5f52n1diw7yf51dkfpyq2fjrbf9hjxdpr81fpllyc";
  };

  nativeBuildInputs = [
    dpkg
    makeWrapper
    autoPatchelfHook
    libelf
    elfutils
  ];

  unpackCmd = ''
    mkdir -p root
    dpkg-deb -x $curSrc root
  '';

  dontBuild = true;

  buildInputs = [
    systemd
    libselinux
    glibc
    audit
    acl
    zlib
    glib
    libnfnetlink
    libelf
    policycoreutils
    libglibutil
    libnetfilter_queue
    license
  ];

  installPhase = ''
    runHook preInstall

    mkdir -p $out/etc/systemd/system
    mkdir -p $out/usr/bin
    mkdir -p $out/var/log/microsoft
    mkdir -p $out/var/opt/microsoft
    mkdir -p $out/usr/lib/sysusers.d
    mkdir -p $out/opt

    cp -r opt/ $out

    # prepare the systemd service
    install -m644 $out/opt/microsoft/${pname}/conf/mdatp.service $out/etc/systemd/system/mdatp.service
    # fix the links in systemd service
    substituteInPlace $out/etc/systemd/system/mdatp.service \
      --replace /opt/ $out/opt/

    # prepare executable
    ln -sf $out/opt/microsoft/${pname}/sbin/wdavdaemonclient $out/usr/bin/mdatp

    # add license
    mkdir -p $out/etc/opt/microsoft/${pname}
    cp ${license}/mdatp_onboard.json $out/etc/opt/microsoft/mdatp/mdatp_onboard.json

    runHook postInstall
  '';
}
