{
  lib,
  config,
  pkgs,
  ...
}:
let
  mdatp = pkgs.callPackage ./build-mdatp.nix { };
in
{
  environment.systemPackages = [ mdatp ];

  systemd.services.mdatp = {
    enable = true;
    description = "Microsoft Defender";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    environment = {
      LD_LIBRARY_PATH = "${mdatp}/opt/microsoft/mdatp/lib";
      MALLOC_ARENA_MAX = "2";
      ENABLE_CRASHPAD = "1";
    };
    serviceConfig = {
      Type = "simple";
      WorkingDirectory = "${mdatp}/opt/microsoft/mdatp/sbin";
      LimitNOFILE = 65536;
      ExecStart = "${mdatp}/opt/microsoft/mdatp/sbin/wdavdaemon";
      NotifyAccess = "main";
      Restart = "always";
      StartLimitInterval = 120;
      StartLimitBurst = 3;
      DELEGATE = "yes";
    };
  };
}
