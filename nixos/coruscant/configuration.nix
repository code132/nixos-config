# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{
  modulesPath,
  config,
  lib,
  pkgs,
  ...
}:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")

    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./disk-config.nix
    ./nextcloud.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    efiSupport = true;
    efiInstallAsRemovable = true;
  };

  networking.hostName = "coruscant";

  time.timeZone = "Europe/Berlin";

  i18n.defaultLocale = "en_US.UTF-8";

  programs.fish.enable = true;
  users.mutableUsers = false;
  users.users.root = {
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN3yuU0uZs3uAlQBBepOpn4zJdc/4i+RZlLgZZ6GthG9 loginkey for strato vc4-8"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKCHTvYuc/csTYpyFp69FHioj5uProv3esST/43iyVmH jonas@nixosDesktop"
    ];
    initialHashedPassword = "$7$CU..../....FYpI4w4fs/NEYhmWVGfqS/$Rimmcg9MZLf6w8lpuGNhqhsUyOqTFIUdp5nWBzbON2D";
  };
  users.users.jonas = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    shell = pkgs.fish;
    uid = 1000;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKCHTvYuc/csTYpyFp69FHioj5uProv3esST/43iyVmH jonas@nixosDesktop"
    ];
    initialHashedPassword = "$7$CU..../....udiLJukolIN4YnAK43aBt1$ObNyJ7vIDFM2KSj76rxa0L8/oWQ5kIwZYcajQhsrdIB";
  };

  # List packages installed in system profile. To search, run:
  environment.systemPackages = map lib.lowPrio [
    pkgs.vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    pkgs.wget
    pkgs.curl
    pkgs.gitMinimal
  ];

  services = {
    openssh.enable = true;
    openssh.settings.PasswordAuthentication = false;
    fail2ban.enable = true;
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;

  security = {
    lockKernelModules = lib.mkDefault true;
  };

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.11"; # Did you read the comment?

}
