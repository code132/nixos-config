{
  config,
  lib,
  pkgs,
  ...
}:
{
  networking.firewall.allowedTCPPorts = [
    80
    443
  ];

  services.nginx = {
    enable = true;
    virtualHosts.${config.services.nextcloud.hostName} = {
      forceSSL = true;
      enableACME = true;
    };
  };

  security.acme = {
    acceptTerms = true;
    certs = {
      ${config.services.nextcloud.hostName}.email = "nextcloud@jonasweissensel.de";
    };
  };

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud29;
    hostName = "nextcloud.jonasweissensel.de";
    https = true;
    configureRedis = true;
    database.createLocally = true;
    # phpExtraExtensions = all: [];

    maxUploadSize = "16G";
    autoUpdateApps.enable = true;
    extraAppsEnable = true;
    extraApps = {
      inherit (config.services.nextcloud.package.packages.apps)
        calendar
        contacts
        deck
        forms
        gpoddersync
        mail
        notify_push
        notes
        memories
        polls
        cospend
        qownnotesapi
        tasks
        ;
    };

    config = {
      adminuser = "admin";
      adminpassFile = "/etc/nextcloud-admin-pass";
      dbtype = "pgsql";
    };
    settings = {
      default_phone_region = "DE";
      trusted_domains = [ "nextcloud.jonasweissensel.de" ];
    };
  };
}
