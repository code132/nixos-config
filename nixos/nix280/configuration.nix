# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  self,
  ...
}: {
  # You can import other NixOS modules here
  imports = [
    # If you want to use modules your own flake exports (from modules/nixos):
    # outputs.nixosModules.example

    # Or modules from other flakes (such as nixos-hardware):
    # inputs.hardware.nixosModules.common-cpu-amd
    # inputs.hardware.nixosModules.common-ssd
    inputs.agenix.nixosModules.default
    inputs.agenix-rekey.nixosModules.default

    # You can also split up your configuration and import pieces of it here:
    # ./users.nix
    ./yubikey.nix

    # Import your generated (nixos-generate-config) hardware configuration
    ./hardware-configuration.nix
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.stable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default
      inputs.agenix-rekey.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Only allow unfree packages named here
      allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) ["discord"];
      permittedInsecurePackages = [
        "olm-3.2.16"
      ];
    };
  };

  # This will add each flake input as a registry
  # To make nix3 commands consistent with your flake
  nix.registry =
    (lib.mapAttrs (_: flake: {inherit flake;}))
    ((lib.filterAttrs (_: lib.isType "flake")) inputs);

  # This will additionally add your inputs to the system's legacy channels
  # Making legacy nix commands consistent as well, awesome!
  nix.nixPath = ["/etc/nix/path"];
  environment.etc =
    lib.mapAttrs' (name: value: {
      name = "nix/path/${name}";
      value.source = value.flake;
    })
    config.nix.registry;

  nix.settings = {
    # Enable flakes and new 'nix' command
    experimental-features = "nix-command flakes";
    # Deduplicate and optimize nix store
    auto-optimise-store = true;
    access-tokens = "include /etc/nixos/secrets/github-api-token.txt";
    # make agenix-rekey work
    extra-sandbox-paths = ["/var/tmp/agenix-rekey"];
    extra-trusted-users = ["@wheel"];
  };
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  boot = {
    # extraModprobeConfig = ''
    # options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
    # '';
    # extraModulePackages = with config.boot.kernelPackages; [
    # v4l2loopback.out
    # ];
    initrd.systemd.enable = true;
    # kernelModules = ["v4l2loopback"];
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = ["quiet"];
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    plymouth.enable = true;
    kernel.sysctl = {"kernel.sysrq" = 1;};
  };

  age = {
    secrets.homevpn-wireguard-private-key.rekeyFile = ../../secrets/homevpn-wireguard-private-key.age;
    secrets.homevpn-wireguard-preshared-key.rekeyFile = ../../secrets/homevpn-wireguard-preshared-key.age;
  };
  age.rekey = {
    # see
    # this enables the old behavior of age-rekey. consider changing to "local" in the future;
    storageMode = "derivation";
    cacheDir = "/var/tmp/agenix-rekey/${builtins.toString config.users.users.jonas.uid}";
    masterIdentities = ["/home/jonas/.ssh/age-yubikey-identity-c1d3721d.txt"];
    hostPubkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMSiW/SER1OmZBLeNZy374aKlgca/BSHECZm6h0vRBEc nix280 host key";
    generatedSecretsDir = "./secrets/generated";
  };

  console = {
    font = "Lat2-Terminus16";
    #   keyMap = "us";
    useXkbConfig = true; # use xkbOptions in tty.
  };

  environment.shells = [pkgs.fish];
  environment.systemPackages = with pkgs; [
    # from inputs
    # inputs.xob-tools.packages.${system}.read-brightness
    inputs.agenix-rekey.packages.${system}.default
    # from nixpkgs
    # system stuff
    pciutils
    usbutils
    file
    aha
    clinfo
    glxinfo
    vulkan-tools
    wayland-utils
    # user
    neovim
    wget
    git
    helix
    firefox
    chrysalis

    brightnessctl

    # kde
    libsForQt5.bismuth
  ];

  fonts.packages = with pkgs; [
    # fonts
    nerd-fonts.mplus
    nerd-fonts.fira-code
    nerd-fonts.hack
    nerd-fonts.terminess-ttf
    nerd-fonts.iosevka-term
    font-awesome
    fira-code-symbols
    cozette
    source-code-pro
    fixedsys-excelsior
    ibm-plex
    # mplus-outline-fonts.githubRelease
    # iosevka
  ];

  hardware = {
    # pulseaudio.enable = true;
    # pulseaudio.package = pkgs.pulseaudioFull;
    bluetooth.enable = true;
    bluetooth.settings = {
      General = {
        ControllerMode = "dual";
        DiscoverableTimeout = 180;
        FastConnectable = true;
        Experimental = true;
      };
      Policy = {
        ReconnectAttempts = 7;
        ReconnectIterals = "1,2,4,8,16,32,64";
        AutoEnable = true;
      };
    };
    bluetooth.input = {General = {UserspaceHID = true;};};
    keyboard.qmk.enable = true;
    ledger.enable = true;
  };

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  networking.hostName = "nix280"; # Define your hostname.

  networking.wireless.iwd.enable = true; # Enables wireless support via wpa_supplicant.
  networking.networkmanager.wifi.backend = "iwd";
  networking.networkmanager.enable = true; # Easiest to use and most distros use this by default.
  networking.networkmanager.ensureProfiles.profiles = {
    "37C3" = {
      connection = {
        id = "37C3";
        type = "wifi";
        interface-name = "wlan0";
      };
      wifi = {
        mode = "infrastructure";
        ssid = "37C3";
      };
      wifi-security = {
        auth-alg = "open";
        key-mgmt = "wpa-eap";
      };
      "802-1x" = {
        anonymous-identity = "37C3";
        eap = "ttls;";
        identity = "37C3";
        password = "37C3";
        phase2-auth = "mschapv2";
      };
      ipv4 = {method = "auto";};
      ipv6 = {
        addr-gen-mode = "default";
        method = "auto";
      };
    };
  };
  networking.wg-quick.interfaces = {
    wg0 = {
      autostart = true;
      address = ["192.168.42.201/24"];
      dns = ["192.168.42.1" "fritz.box"];
      privateKeyFile = config.age.secrets.homevpn-wireguard-private-key.path;
      peers = [
        {
          publicKey = "bos+0xJ7yakqShFVFNAkVvhWBA3qT+Om82X0KqrInVI=";
          presharedKeyFile =
            config.age.secrets.homevpn-wireguard-preshared-key.path;
          allowedIPs = ["192.168.42.0/24" "0.0.0.0/0"];
          endpoint = "zorwt9i51u805sob.myfritz.net:57180";
          persistentKeepalive = 25;
        }
      ];
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs = {
    adb.enable = true;
    # hyprland.enable = true;
    river.enable = true;
    river.extraPackages = with pkgs; [rivercarro swaylock foot dmenu rofi];
    fish.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    dconf.enable = true;
    tmux = {
      enable = true;
      clock24 = true;
    };
    nm-applet.enable = true;
    # xss-lock = {
    # enable = true;
    # lockerCommand = "{pkgs.betterscreenlock}/bin/betterscreenlock -l";
    # };
    yubikey-touch-detector.enable = true;
  };

  users = {
    mutableUsers = false;
    users.root.initialHashedPassword = "$y$j9T$T4I/iyyACThyO7iX5BXQJ1$IhaZNg.7qT/XFFORD5grVuNdSuo9es2afSFvPY36VR1";
    users.jonas = {
      initialHashedPassword = "$y$j9T$96a3PUDmBBmkmEKTCizDq0$tTfOtAZ7GvsN0fVXIZXvdpkMJVbBLJlZ8pYoN0QjqJB";
      isNormalUser = true;
      extraGroups = [
        "adbusers"
        "audio"
        "docker"
        "dialout"
        "networkmanager"
        "uucp"
        "wheel"
        "video"
      ];
      shell = pkgs.fish;
      uid = 1000;
    };
  };

  security = {
    pam.services.kwallet = {
      name = "kwallet";
      enableKwallet = true;
    };
    rtkit.enable = true;
  };

  services = {
    # Enable bluetooth manager
    blueman.enable = true;
    fwupd.enable = true;
    # Enable gnome-keyring for ssh agent
    gnome.gnome-keyring.enable = true;
    pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
      jack.enable = true;
      wireplumber.enable = true;
    };

    # Enable CUPS to print documents.
    printing.enable = true;
    printing.drivers = with pkgs; [gutenprint brlaser];

    openssh = {
      enable = true;
      settings = {
        # Forbid root login through SSH.
        PermitRootLogin = "no";
        # Use keys only. Remove if you want to SSH using password (not recommended)
        PasswordAuthentication = false;
      };
      # host keys are necessary for agenix-rekey
      hostKeys = [
        {
          type = "ed25519";
          rounds = 100;
          path = "/etc/ssh/ssh_host_ed25519_key";
          comment = "nix280 host key";
        }
        {
          bits = 4096;
          path = "/etc/ssh/ssh_host_rsa_key";
          type = "rsa";
        }
      ];
    };

    # Enable throttled for better thermo and power management
    # TODO: is this part of nixos-hardware already?
    throttled.enable = true;
    throttled.extraConfig = ''
      [GENERAL]
      # Enable or disable the script execution
      Enabled: True
      # SYSFS path for checking if the system is running on AC power
      Sysfs_Power_Path: /sys/class/power_supply/AC*/online
      # Auto reload config on changes
      Autoreload: True

      ## Settings to apply while connected to Battery power
      [BATTERY]
      # Update the registers every this many seconds
      Update_Rate_s: 30
      # Max package power for time window #1
      PL1_Tdp_W: 29
      # Time window #1 duration
      PL1_Duration_s: 28
      # Max package power for time window #2
      PL2_Tdp_W: 44
      # Time window #2 duration
      PL2_Duration_S: 0.002
      # Max allowed temperature before throttling
      Trip_Temp_C: 85
      # Set cTDP to normal=0, down=1 or up=2 (EXPERIMENTAL)
      cTDP: 0
      # Disable BDPROCHOT (EXPERIMENTAL)
      Disable_BDPROCHOT: False

      ## Settings to apply while connected to AC power
      [AC]
      # Update the registers every this many seconds
      Update_Rate_s: 5
      # Max package power for time window #1
      PL1_Tdp_W: 44
      # Time window #1 duration
      PL1_Duration_s: 28
      # Max package power for time window #2
      PL2_Tdp_W: 44
      # Time window #2 duration
      PL2_Duration_S: 0.002
      # Max allowed temperature before throttling
      Trip_Temp_C: 95
      # Set HWP energy performance hints to 'performance' on high load (EXPERIMENTAL)
      # Uncomment only if you really want to use it
      # HWP_Mode: False
      # Set cTDP to normal=0, down=1 or up=2 (EXPERIMENTAL)
      cTDP: 0
      # Disable BDPROCHOT (EXPERIMENTAL)
      Disable_BDPROCHOT: False

      # All voltage values are expressed in mV and *MUST* be negative (i.e. undervolt)!
      [UNDERVOLT.BATTERY]
      # CPU core voltage offset (mV)
      CORE: -105
      # Integrated GPU voltage offset (mV)
      GPU: -85
      # CPU cache voltage offset (mV)
      CACHE: -105
      # System Agent voltage offset (mV)
      UNCORE: -85
      # Analog I/O voltage offset (mV)
      ANALOGIO: 0

      # All voltage values are expressed in mV and *MUST* be negative (i.e. undervolt)!
      [UNDERVOLT.AC]
      # CPU core voltage offset (mV)
      CORE: 0
      # Integrated GPU voltage offset (mV)
      GPU: 0
      # CPU cache voltage offset (mV)
      CACHE: 0
      # System Agent voltage offset (mV)
      UNCORE: 0
      # Analog I/O voltage offset (mV)
      ANALOGIO: 0

      # [ICCMAX.AC]
      # # CPU core max current (A)
      # CORE:
      # # Integrated GPU max current (A)
      # GPU:
      # # CPU cache max current (A)
      # CACHE:

      # [ICCMAX.BATTERY]
      # # CPU core max current (A)
      # CORE:
      # # Integrated GPU max current (A)
      # GPU:
      # # CPU cache max current (A)
      # CACHE:
    '';
    trezord.enable = true;
    desktopManager.plasma6.enable = true;
    displayManager.sddm.enable = true;
    displayManager.sddm.wayland.enable = true;
    displayManager.defaultSession = "plasma";
    libinput.enable = true;
    xserver = {
      enable = true;

      # Enable the Plasma 5 Desktop Environment.

      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
        #config = builtins.readFile ../../users/jonas/xmonad/xmonad.hs;
        config = null;
        extraPackages = hp: [
          hp.xmonad-dbus
          hp.dbus
          hp.monad-logger
          hp.xmonad-contrib
        ];
      };

      # Configure keymap in X11
      xkb.layout = "us_intl";
      xkb.options = "eurosign:e,ctrl:hyper_capscontrol,terminate:ctrl_alt_bksp,compose:ralt";
      # Enable touchpad support (enabled default in most desktopManager).
    };
    udev.packages = [
      pkgs.chrysalis
      (pkgs.callPackage ../../modules/nixos/wchisp-udev-rules {})
    ];
    udev.extraRules = ''
      SUBSYSTEM=="usb", ATTR{idVendor}=="4348", ATTR{idProduct}=="55e0", MODE="0666"
      SUBSYSTEM=="usb", ATTR{idVendor}=="abba", ATTR{idProduct}=="0001", MODE="0666"
    '';
    upower.enable = true;
  };

  # SystemD
  systemd.services.upower.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Virtualisation
  # virtualisation.libvirtd.enable = true;
  virtualisation.podman.enable = true;
  virtualisation.podman.dockerCompat = true;

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  system.stateVersion = "22.11";
  system.configurationRevision =
    if self ? rev
    then self.rev
    else "dirty";
}
