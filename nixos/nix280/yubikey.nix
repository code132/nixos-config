{
  config,
  lib,
  pkgs,
  ...
}:
{
  programs.ssh.startAgent = false;
  services.pcscd.enable = true;

  environment.shellInit = ''
    gpg-connect-agent /bye
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
  '';

  environment.systemPackages = with pkgs; [
    # yubioath-flutter
    yubikey-manager-qt
    yubikey-personalization
    yubikey-personalization-gui
  ];

  services.udev.packages = with pkgs; [ yubikey-personalization ];
}
