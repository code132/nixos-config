# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{ inputs, outputs, config, pkgs, lib, ... }:
# let
#   opalServices = [
#     "search"
#     "product"
#     "changes"
#     "offering"
#     "archive"
#     "availability"
#     "statistics"
#     "dashboard"
#     "grafana"
#   ];
#   opalDomains = lib.concatMapStringsSep ''\
#     '' (service: "${service}.develop.opal.cloud.otto.de ${service}.live.opal.cloud.otto.de ") opalServices;
# in
{
  imports = [
    # If you want to use modules your own flake exports (from modules/nixos):
    # outputs.nixosModules.example

    # Or modules from other flakes (such as nixos-hardware):
    # inputs.hardware.nixosModules.common-cpu-amd
    # inputs.hardware.nixosModules.common-ssd
    # inputs.agenix.nixosModules.default
    # inputs.agenix-rekey.nixosModules.default

    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.stable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default
      # inputs.agenix-rekey.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Only allow unfree packages named here
      allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [ "discord" ];
      permittedInsecurePackages = [
        "olm-3.2.16" # nheko
      ];

    };
  };

  # This will add each flake input as a registry
  # To make nix3 commands consistent with your flake
  nix.registry = (lib.mapAttrs (_: flake: { inherit flake; }))
    ((lib.filterAttrs (_: lib.isType "flake")) inputs);
  # This will additionally add your inputs to the system's legacy channels
  # Making legacy nix commands consistent as well, awesome!
  nix.nixPath = [ "/etc/nix/path" ];
  environment.etc = lib.mapAttrs' (name: value: {
    name = "nix/path/${name}";
    value.source = value.flake;
  }) config.nix.registry;

  nix.settings = {
    # Enable flakes and new 'nix' command
    experimental-features = [ "nix-command" "flakes" ];
    # Deduplicate and optimize nix store
    auto-optimise-store = true;
    # FIXME: provide token via agenix
    # access-tokens = "include /etc/nixos/secrets/github-api-token.txt";
    # make agenix-rekey work
    # extra-sandbox-paths = ["/var/tmp/agenix-rekey"];
  };
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  boot = {
    # Use the systemd-boot EFI boot loader.
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    extraModulePackages = [
      config.boot.kernelPackages.v4l2loopback.out
      config.boot.kernelPackages.nct6687d
      # pkgs.linuxPackages.v4l2loopback
    ];
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [
      "kvm-amd"
      # Virtual Camera
      "v4l2loopback"
      # Virtual Microphone, built-in
      "snd-aloop"
      # Fix for lm_sensors for Nuvoton NCT6687-R chipset
      "nct6687"
    ];
    kernelParams = [ "quiet" ];
    plymouth.enable = true;
    initrd.kernelModules = [ "amdgpu" ];
    extraModprobeConfig = ''
      # exclusive_caps: Skype, Zoom, Teams etc. will only show device when actually streaming
      # card_label: Name of virtual camera, how it'll show up in Skype, Zoom, Teams
      # https://github.com/umlaeute/v4l2loopback
      options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
    '';
  };

  networking.hostName = "nixosDesktop"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  # networking.interfaces.enp2s0f0u3u4u4.useDHCP = true;
  networking.interfaces.enp42s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n = let
    ENGLISH = "en_US.UTF-8";
    GERMAN = "de_DE.UTF-8";
  in {
    defaultLocale = ENGLISH;
    supportedLocales = map (lang: "${lang}/UTF-8") [ ENGLISH GERMAN ];
  };
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Enable the X11 windowing system.
  # hardware.nvidia.modesetting.enable = true;
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = [ pkgs.amdvlk pkgs.rocmPackages.clr.icd ];
  # hardware.opengl.extraPackages = with pkgs; [ amdvlk rocm-opencl-icd rocm-opencl-runtime ];
  # hardware.xpadneo.enable = true; # xbox controller
  # hardware.opengl = {
  # extraPackages = with pkgs; [ vaapiVdpau libvdpau-va-gl ];
  # };
  services.dbus.enable = true;
  # might improve DNS resolution with GlobalProtect VPN
  services.nscd.enableNsncd = true;

  services.desktopManager.plasma6.enable = true;
  # Enable the GNOME Desktop Environment.
  services = {
    displayManager.sddm = {
      enable = true;
      wayland.enable = true;
    };
    displayManager.defaultSession = "plasma";
  };
  services.xserver = {
    enable = true;
    videoDrivers = [ "amdgpu" "vesa" ];
    # desktopManager.gnome.enable = true;
    # displayManager.gdm = {
    # enable = true;
    # wayland = true;
    # };
    # };
    # services.gnome = {
    # gnome-browser-connector.enable = true;
    # gnome-keyring.enable = true;
    # gnome-online-accounts.enable = true;
    # evolution-data-server.enable = true;
    # gnome-remote-desktop.enable = true;
    # gnome-settings-daemon.enable = true;
    # tracker.enable = true;
    # tracker-miners.enable = true;
  };

  programs.gnupg.agent = {
    enable = false;
    enableSSHSupport = false;
  };
  services.pcscd.enable = true;

  # Configure keymap in X11
  services.xserver.xkb.layout = "us";
  services.xserver.xkb.options = "eurosign:e";

  hardware.logitech.wireless.enable = true;
  hardware.logitech.wireless.enableGraphical = true;
  hardware.keyboard.qmk.enable = true;
  hardware.keyboard.zsa.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true; # pycurl is broken for now
  services.printing.drivers = with pkgs; [
    brlaser
    brgenml1lpr
    brgenml1cupswrapper
  ];

  hardware.pulseaudio.enable = false;
  # hardware.pulseaudio.support32Bit = true;
  # hardware.pulseaudio.configFile = pkgs.runCommand "default.pa" {} ''
  # sed 's/module-udev-detect$/module-udev-detect tsched=0/' \
  # ${pkgs.pulseaudio}/etc/pulse/default.pa > $out
  # '';
  # use pipewire (https://nixos.wiki/wiki/PipeWire)
  # rtkit is optional but recommended
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse = { enable = true; };
    # wireplumber is recommended by official pipewire wiki
    wireplumber.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  services.trezord.enable = true;
  hardware.ledger.enable = true;
  services.udev.packages = [
    pkgs.via
    pkgs.qmk-udev-rules
    pkgs.libu2f-host
    pkgs.yubikey-personalization
  ];
  services.udev.extraRules = ''
    # WCH ISP bootloader
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="4348", ATTRS{idProduct}=="55e0", GROUP="plugdev", MODE="0666"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="abba", ATTRS{idProduct}=="0001", GROUP="plugdev", MODE="0666"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="abba", ATTRS{idProduct}=="0100", GROUP="plugdev", MODE="0666"
    # Rules for atreus from keyboard.io
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="2302", SYMLINK+="Atreus2", ENV{ID_MM_DEVICE_IGNORE}:="1", ENV{ID_MM_CANDIDATE}:="0", TAG+="uaccess", TAG+="seat"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="2303", SYMLINK+="Atreus2", ENV{ID_MM_DEVICE_IGNORE}:="1", ENV{ID_MM_CANDIDATE}:="0", TAG+="uaccess", TAG+="seat"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE:="0666", SYMLINK+="stm32_dfu"
  '';
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = false;
    users.jonas = {
      isNormalUser = true;
      shell = pkgs.fish;
      extraGroups = [
        "adbusers"
        "libvirtd"
        "wheel"
        "audio"
        "lp"
        "video"
        "dialout"
        "camera"
        "plugdev"
      ];
      initialHashedPassword =
        "$7$CU..../....IZTo9uJ.CflxVKb.XxkTh1$nekADu.68LBDqGhdgxCVdj7630TObgd0uAQbaeT0IM4";
      packages = [ pkgs.lutris pkgs.mc ];
      uid = 1000;
    };
    extraGroups.vboxusers.members = [ "jonas" ];
  };

  services.syncthing = {
    enable = true;
    user = "jonas";
    dataDir = "/home/jonas/Syncthing";
    configDir = "/home/jonas/.config/syncthing";
    settings.gui = {
      user = "jonas";
      password = "-just-a-placeholder-string-";
    };
  };
  services.samba = {
    enable = true;
    openFirewall = true;
    nsswins = true;
  };
  # Enable Web Services Dynamic Discovery host daemon. This enables (Samba) hosts, like your local NAS device, to be found by Web Service Discovery Clients like Windows.
  services.samba-wsdd.enable = true;
  services.clamav = {
    daemon.enable = true;
    updater.enable = true;
  };
  # why the ports?
  networking.firewall.enable = true;
  # Syncthing ports: 8384 for remote access to GUI
  # 22000 TCP and/or UDP for sync traffic
  # 21027/UDP for discovery
  # source: https://docs.syncthing.net/users/firewall.html
  networking.firewall.allowedTCPPorts = [ 22000 1716 5357 ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 3702 ];

  fileSystems = {
    "/mnt/terramaster/daten" = {
      device = "//192.168.42.34/daten";
      fsType = "cifs";
      options = let
        automount_opts =
          "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${
          builtins.toString config.users.users.jonas.uid
        },gid=${builtins.toString config.users.groups.users.gid}"
      ];
    };
    "/mnt/terramaster/music" = {
      device = "//192.168.42.34/music";
      fsType = "cifs";
      options = let
        automount_opts =
          "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${
          builtins.toString config.users.users.jonas.uid
        },gid=${builtins.toString config.users.groups.users.gid}"
      ];
    };
    "/mnt/terramaster/photo" = {
      device = "//192.168.42.34/photo";
      fsType = "cifs";
      options = let
        automount_opts =
          "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${
          builtins.toString config.users.users.jonas.uid
        },gid=${builtins.toString config.users.groups.users.gid}"
      ];
    };
    "/mnt/terramaster/video" = {
      device = "//192.168.42.34/video";
      fsType = "cifs";
      options = let
        automount_opts =
          "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${
          builtins.toString config.users.users.jonas.uid
        },gid=${builtins.toString config.users.groups.users.gid}"
      ];
    };
  };
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    nssmdns
    killall
    nixVersions.latest
    # gnomeExtensions.appindicator
    # gnomeExtensions.gsconnect
    # gnomeExtensions.vitals
    # gnomeExtensions.hide-activities-button
    # gnomeExtensions.espresso
    pinentry-gnome3
    pinentry
    dmenu
    ghostscript
    rxvt-unicode
    xterm
    ntfs3g

    # for gaming / windows only games
    # wineWowPackages.waylandFull
    # winetricks

    virt-manager
    spice
    spice-gtk
    # xboxdrv
    # python310
    via
  ];

  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';
  environment.pathsToLink = [ "/libexec" "/share/nix-direnv" ];
  # environment.etc.hosts.mode = "0644";
  programs = {
    adb.enable = true;

    fish.enable = true;
    partition-manager.enable = true;
    steam = {
      enable = true;
      gamescopeSession.enable = true;
      remotePlay.openFirewall = true;
    };
    # Some programs need SUID wrappers, can be configured further or are
    # started in user sessions.
    # programs.mtr.enable = true;
    # programs.gnupg.agent = {
    #   enable = true;
    #   enableSSHSupport = true;
    # };
    dconf.enable = true;
    gpaste.enable = true;
    tmux = {
      enable = true;
      clock24 = true;
    };
    yubikey-touch-detector.enable = true;
  };
  # List services that you want to enable:
  # services.globalprotect.enable = true;
  # services.globalprotect.csdWrapper = "${pkgs.openconnect}/libexec/openconnect/hipreport.sh";
  # services.globalprotect.settings = {
  #   "*" = {
  #     openconnect-args = ''
  #       --script="${pkgs.vpn-slice}/bin/vpn-slice 80.85.0.0/16 docs.infrastructure.seo2.cloud.otto.de \
  #         launchpad.ottogroup.com billing.infrastructure.seo2.cloud.otto.de it-shop.ocn-ottogroup.com \
  #         alarm-dashboard.infrastructure.seo2.cloud.otto.de grafana.infrastructure.seo2.cloud.otto.de \
  #         ocnhhsam003.ocn.ottogroup.com auth.live.si.cloud.otto.de biprep01.ocn.ottogroup.com \
  #         exa.prd.exasol.database.bi.otto.de exa.dev.exasol.database.bi.otto.de fwauth.ov.otto.de \
  #         mercado-online.ov.otto.de \
  #         ${opalDomains} \
  #         -d ocn.ottogroup.com -d ov.otto.de -d int-ad.hermes-ws.com"
  #     '';
  #   };
  # };
  # see https://search.nixos.org/options?channel=unstable&show=security.pki.certificates&from=0&size=50&sort=relevance&type=packages&query=pki
  # for documentation
  # security.pki.certificates = with builtins; [
  #   # Otto Root CA (old)
  #   (readFile ./otto/OTTO-Root-CA-v01.txt)
  #   # Otto Company CA1 (old)
  #   (readFile ./otto/OTTO-Company-CA1-v01.txt)
  #   # Otto Universal Issuing CA1 (old)
  #   (readFile ./otto/OTTO-Universal-Issuing-CA1-v01.txt)

  #   # Ottogroup-Root-CA-v01
  #   (readFile ./otto/Ottogroup-Root-CA-v01.pem.txt)
  #   # Ottogroup-Issuing0201-CA-v01
  #   (readFile ./otto/Ottogroup-Issuing0201-CA-v01.pem.txt)
  #   # Ottogroup-Issuing0301-CA-v01
  #   (readFile ./otto/Ottogroup-Issuing0301-CA-v01.pem.txt)
  # ];

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # FlatPak
  services.flatpak.enable = true;

  # Virtualization
  # spice needs setuid on helper
  # https://github.com/NixOS/nixpkgs/blob/nixos-22.05/pkgs/development/libraries/spice-gtk/default.nix#L132
  security.wrappers = {
    spice-client-glib-usb-acl-helper = {
      source = "${pkgs.spice-gtk}/bin/spice-client-glib-usb-acl-helper";
      owner = "root";
      group = "root";
      setuid = true;
    };
  };
  virtualisation.libvirtd.enable = true;
  # virtualisation.docker.enable = true;
  # virtualisation.docker.rootless = {
  # enable = true;
  # setSocketVariable = true;
  # };
  # virtualisation.docker.storageDriver = "btrfs";
  virtualisation.podman.enable = true;
  virtualisation.podman.dockerCompat = true;
  # virtualisation.virtualbox.host.enable = true;
  # virtualisation.virtualbox.host.enableExtensionPack = true;

  # https://nixos.wiki/wiki/Fonts
  fonts.packages = with pkgs; [
    font-awesome
    nerd-fonts.blex-mono
    nerd-fonts.mplus
    nerd-fonts.fira-code
    nerd-fonts.hack
    nerd-fonts.terminess-ttf
    nerd-fonts.iosevka-term
    google-fonts
    fira-code-symbols
    open-sans
    libertinus
    ttf-envy-code-r
    victor-mono
    mononoki
    monoid
    # bitmap fonts
    proggyfonts
    cozette
    envypn-font
  ];

  # use btrbk for time machine style backups
  services.btrbk = {
    instances = {
      btrbk.settings = {
        snapshot_preserve = "14d";
        snapshot_preserve_min = "2d";
        volume = {
          "/mnt/btr_pool" = { subvolume.home.snapshot_create = "onchange"; };
        };
      };
    };
  };
  services.fstrim.enable = true;

  system.autoUpgrade.enable = true;
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}
