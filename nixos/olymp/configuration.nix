# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).
{
  config,
  lib,
  pkgs,
  ...
}:
{
  imports =
    # let
    # impermanence = builtins.fetchTarball "https://github.com/nix-community/impermanence/archive/master.tar.gz";
    # disko = builtins.fetchTarball "https://github.com/nix-community/disko/archive/master.tar.gz";
    # in
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # "${disko}/module.nix"
      # "${impermanence}/nixos.nix"
      ./disko-config.nix
      # ./immich.nix
      ./nextcloud.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  boot.supportedFilesystems = [ "zfs" ];

  # hardware
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      intel-vaapi-driver
      libvdpau-va-gl
    ];
  };
  environment.sessionVariables = {
    LIBVA_DRIVER_NAME = "iHD";
  };
  # nixpkgs configuration
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [
    "olm-3.2.16"
  ];

  networking.hostName = "olymp";
  networking.hostId = "5ccabde2";

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
    # useXkbConfig = true; # use xkb.options in tty.
  };

  # services.printing.enable = true;

  users = {
    mutableUsers = false;
    users.root.initialHashedPassword = "$y$j9T$YXTR.cbdiKAq8xEiRJ2jQ.$MmayLZiD8Ersd2xw3h2sutc02zUqc56/9d4SNHz8n15";
    users.root.shell = pkgs.fish;
    users.jonas = {
      isNormalUser = true;
      extraGroups = [
        "libvirtd"
        "lp"
        "wheel"
      ]; # Enable ‘sudo’ for the user.
      initialHashedPassword = "$y$j9T$MiFF8fIJvQGt1kfB7AjUx1$lgEw/bPVow/mDBjjzZWKSx.MkaEVDgahm2N3IVY3UT0";
      packages = [ pkgs.tree ];
      shell = pkgs.fish;
      uid = 1000;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKCHTvYuc/csTYpyFp69FHioj5uProv3esST/43iyVmH jonas"
        "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIDTnFLTlUrDYoDEifb1vlekyUAcx/5P53V7OSYRHpxzmAAAABHNzaDo= nix280@jonasweissensel.de"
        "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIPsaF/FkTWNEXCrCtT+GstwZD94jBxqcDzLDxp8pCxnwAAAABHNzaDo= jonas@nix280"
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    curl
    more
    usbutils

    cifs-utils

    # for jellyfin
    pkgs.jellyfin
    pkgs.jellyfin-web
    pkgs.jellyfin-ffmpeg
  ];

  programs.fish.enable = true;
  programs.tmux.enable = true;
  # List services that you want to enable:

  # Samba
  fileSystems."/mnt/terramaster/photo" = {
    device = "//192.168.42.34/photo";
    fsType = "cifs";
    options =
      let
        automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in
      [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${builtins.toString config.users.users.jonas.uid},gid=${builtins.toString config.users.groups.users.gid}"
      ];
  };
  fileSystems."/mnt/terramaster/music" = {
    device = "//192.168.42.34/music";
    fsType = "cifs";
    options =
      let
        automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in
      [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${builtins.toString config.users.users.jonas.uid},gid=${builtins.toString config.users.groups.users.gid}"
      ];
  };
  fileSystems."/mnt/terramaster/daten" = {
    device = "//192.168.42.34/daten";
    fsType = "cifs";
    options =
      let
        automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in
      [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${builtins.toString config.users.users.jonas.uid},gid=${builtins.toString config.users.groups.users.gid}"
      ];
  };
  fileSystems."/mnt/terramaster/video" = {
    device = "//192.168.42.34/video";
    fsType = "cifs";
    options =
      let
        automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in
      [
        "${automount_opts},credentials=/etc/nixos/smb-secret,uid=${builtins.toString config.users.users.jonas.uid},gid=${builtins.toString config.users.groups.users.gid}"
      ];
  };

  services = {
    jellyfin = {
      enable = true;
      openFirewall = true;
    };
    navidrome = {
      enable = true;
      openFirewall = true;
      settings = {
        MusicFolder = "/mnt/terramaster/music";
        Address = "0.0.0.0";
        Port = 4533;
      };
    };
    openssh = {
      enable = true;
    };
  };

  systemd.network.networks = {
    "10-enp6s18" = {
      matchConfig.Name = "enp6s18";
      networkConfig.DHCP = "yes";
      linkConfig.RequiredForOnline = "yes";
    };
    "10-ens18" = {
      matchConfig.Name = "ens18";
      networkConfig.DHCP = "yes";
      linkConfig.RequiredForOnline = "yes";
    };
  };

  virtualisation.oci-containers = {
    backend = "docker";
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

  zramSwap.enable = true;
}
