{ config, lib, pkgs, ... }: {
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud30;
    hostName = "localhost";
    configureRedis = true;
    database.createLocally = true;
    # phpExtraExtensions = all: [];

    maxUploadSize = "16G";
    autoUpdateApps.enable = true;
    extraAppsEnable = true;
    extraApps = {
      inherit (config.services.nextcloud.package.packages.apps)
        calendar contacts deck forms gpoddersync mail notify_push notes memories
        polls cospend qownnotesapi tasks;
    };

    config = {
      adminuser = "admin";
      adminpassFile = "/etc/nextcloud-admin-pass";
      dbtype = "pgsql";
    };
    settings = {
      default_phone_region = "DE";
      trusted_domains = [ "192.168.42.35" ];
    };
  };
}
