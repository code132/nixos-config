# Custom packages, that can be defined similarly to ones from nixpkgs
# You can build them using 'nix build .#example'
{ pkgs, ... }:
{
  # example = pkgs.callPackage ./example { };
  # mdatp = pkgs.callPackage ./mdatp {};
  lolcrab = pkgs.callPackage ./lolcrab { };
  # paloalto-gp = pkgs.callPackage ./paloalgo-gp {};
}
