{
  fetchurl,
  stdenv,
  dpkg,
  makeWrapper,
  systemd,
  libselinux,
  checkpolicy,
  setools,
  libsepol,
  semodule-utils,
  lttng-ust,
  lttng-tools,
  autoPatchelfHook,
  glibc,
  audit,
  acl,
  elfutils,
  zlib,
  glib,
  libglibutil,
  policycoreutils,
  libelf,
  libplist,
  requireFile,
  libsemanage,
  selinux-python,
  python3,
  dmidecode,
}:
let
  license = stdenv.mkDerivation rec {
    pname = "mdatp_onboard.json";
    version = "0";

    src = requireFile {
      name = pname;
      url = /nix/store/qfvprsmkfh36fpwyy2a9bas1fjr9gipv-mdatp_onboard.json;
      sha256 = "0as89q69jj7v7lyzp418qqhl1d9yb3ngw7mwbp4wahs944n263ax";
    };

    dontUnpack = true;
    dontMakeSourcesWritable = true;
    dontPatch = true;
    dontConfigure = true;
    dontBuild = true;

    installPhase = ''
      mkdir -p $out
      cp $src $out/mdatp_onboard.json
    '';
  };
in
stdenv.mkDerivation rec {
  pname = "mdatp";
  version = "101.23112.0009";
  src = fetchurl {
    url = "https://packages.microsoft.com/debian/12/prod/pool/main/m/${pname}/${pname}_${version}_amd64.deb";
    sha256 = "0fk1rv1idf3a01lqc5ivd4f2klfbhr20k7zxn8fvjjhdvjiw74zi";
  };

  nativeBuildInputs = [
    dpkg
    makeWrapper
    autoPatchelfHook
    libelf
    elfutils
    lttng-ust
  ];

  unpackCmd = ''
    mkdir -p root
    dpkg-deb -x $curSrc root
  '';

  dontBuild = true;

  buildInputs = [
    systemd
    libselinux
    checkpolicy
    setools
    libsepol
    semodule-utils
    glibc
    audit
    acl
    zlib
    glib
    # libnfnetlink
    libelf
    lttng-ust
    libplist
    lttng-tools
    libsemanage
    selinux-python
    policycoreutils
    libglibutil
    # libnetfilter_queue
    license
    dmidecode
    python3
  ];

  installPhase = ''
    runHook preInstall

    mkdir -p $out/etc/systemd/system
    mkdir -p $out/usr/bin
    mkdir -p $out/bin
    mkdir -p $out/var/log/microsoft
    mkdir -p $out/var/opt/microsoft
    mkdir -p $out/usr/lib/sysusers.d
    mkdir -p $out/opt

    cp -r opt/ $out

    # prepare the systemd service
    # install -m644 $out/opt/microsoft/${pname}/conf/mdatp.service $out/etc/systemd/system/mdatp.service
    # fix the links in systemd service
    # substituteInPlace $out/etc/systemd/system/mdatp.service \
      # --replace /opt/ $out/opt/

    # prepare executable
    ln -sf $out/opt/microsoft/${pname}/sbin/wdavdaemonclient $out/usr/bin/mdatp
    ln -sf $out/opt/microsoft/${pname}/sbin/wdavdaemonclient $out/bin/mdatp

    for executable in $out/opt/microsoft/${pname}/sbin/* ; do
      ln -sf $executable $out/bin/$(basename "$executable")
    done

    # auditd plugin needs 0750
    install -m=750 $out/opt/microsoft/${pname}/sbin/mdatp_audisp_plugin $out/bin/mdatp_audisp_plugin


    # add license
    mkdir -p $out/etc/opt/microsoft/${pname}
    cp ${license}/mdatp_onboard.json $out/etc/opt/microsoft/mdatp/mdatp_onboard.json

    runHook postInstall
  '';
}
